lexer grammar MetaFLexer;

options {
    superClass = MetaFLexerBase;
}
tokens { INDENT, DEDENT, CURLYCONTENTS }

fragment INDENTATION: (' ' | '\t')* ;
fragment HCHAR: [A-Fa-f0-9];
fragment DIGIT: [0-9] ;
fragment CHAR: [a-zA-Z] | '_' ;

NOT_KEYWORD: 'Not' ;
STATE_LABEL: 'STATE' ;
NAV_LABEL: 'NAV' ;
IF_LABEL: 'IF' ;
DO_LABEL: 'DO' ;

COLON: ':' ;
OPENCURL: '{' ;
CLOSECURL: '}' ;
PLUS: '+'  ;
MINUS: '-' ;

HEX: ('0x')? HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR HCHAR ;

NUMBER: DIGIT+;
DECIMAL: MINUS? DIGIT+ ('.' DIGIT+)? ('E' ('+' | '-') [0-9]+)? ;
IDENTIFIER: CHAR (CHAR | DIGIT | '_')*;

COMMENT: ((NL? WS* '~~' ~('\n')*) | ('/~' .*? '~/')) -> channel(HIDDEN);

NL: ('\r'? '\n' INDENTATION) ;

WS: (' ' | '\t')+;
ANY: . ;

