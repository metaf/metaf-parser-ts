import { Token } from "antlr4ts";
import ModelParser from "../src/modelparser";
import { Meta } from "../src/models/Meta";
import NoneAction from "../src/models/actions/NoneAction";
import NeverCondition from "../src/models/conditions/NeverCondition";
import fs from 'fs';

function setup(afContents: string, filename: string="") : Meta {
  var parser = new ModelParser(afContents);
  var parseResult = parser.tryParse();

  if (!parseResult.success) {
      for (const error of parser.ErrorHandler.Errors) {
        var start = Math.max(0, error.TokenIndex - 10);
        var end = Math.min(parser.Visitor.TokenStream.size - 1, error.TokenIndex + 10);
        var res = "";
        parser.Visitor.TokenStream.seek(error.TokenIndex);
        for (let i = start; i < end; i++) {
          var token = parser.Visitor.TokenStream.get(i);
          var name = parser.Visitor.Parser.vocabulary.getSymbolicName(token.type);
          if (i == error.TokenIndex) {
            res += `\n*<${name}:${token.line}:${token.charPositionInLine}>*\n`;
          }
          else {
            res += `<${name}:${token.line}:${token.charPositionInLine}>`;
          }
        }
          console.error(`Parse Error (${error.TokenIndex}): ${error.Message} @ ${filename} ${error.Line}:${error.Column} (${error.Exception?.message})\n${res}`);
      }
  }
  
  return parseResult.meta;
}

test("can parse basic af file", () => {
  const meta = setup("STATE: {Default}\n\tIF: Never\n\t\tDO: None");

  expect(meta.States.length).toBe(1);
  expect(meta.States[0].Name).toBe("Default");
  expect(meta.States[0].Rules.length).toBe(1);
  expect(meta.States[0].Rules[0].Condition).toBeInstanceOf(NeverCondition);
  expect(meta.States[0].Rules[0].Action).toBeInstanceOf(NoneAction);
});

test("can parse all examples", () => {
  const exampleDir = __dirname + "/../examples/";
  const files = fs.readdirSync(exampleDir);
  for (const file of files) {
    if (file.endsWith(".af")) {
      var meta = setup(fs.readFileSync(exampleDir + file).toString(), file);
    }
  }
})