import { CommonTokenStream } from "antlr4ts";
import { expect } from '@jest/globals';
import { MetaFLexer } from "../gen/MetaFLexer";

export function assertTokenAtPosition(expectedToken: number, tokenIndex: number, tokens: CommonTokenStream, expectedValue: string | null = null) {
  const actualToken = MetaFLexer.VOCABULARY.getSymbolicName(tokens.get(tokenIndex).type);
  expect(actualToken).toBe(MetaFLexer.VOCABULARY.getSymbolicName(expectedToken))
  if (expectedValue != null) {
    expect(tokens.get(tokenIndex).text).toBe(expectedValue);
  }
}