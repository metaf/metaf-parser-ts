import { expect, test } from '@jest/globals';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MetaFLexer } from '../../gen/MetaFLexer';
import { assertTokenAtPosition } from '../testhelpers';
import { setup } from './lexer.tests'

test('If Condition All Tokenization', () => {
  let tokens = setup("All");
  assertTokenAtPosition(MetaFLexer.IF_COND_ALL, 0, tokens);
});

test('If Condition Always Tokenization', () => {
  let tokens = setup("Always");
  assertTokenAtPosition(MetaFLexer.IF_COND_ALWAYS, 0, tokens);
});

test('If Condition Any Tokenization', () => {
  let tokens = setup("Any");
  assertTokenAtPosition(MetaFLexer.IF_COND_ANY, 0, tokens);
});

test('If Condition BlockE Tokenization', () => {
  let tokens = setup("BlockE");
  assertTokenAtPosition(MetaFLexer.IF_COND_BLOCK_E, 0, tokens);
});

test('If Condition BuPercentGE Tokenization', () => {
  let tokens = setup("BuPercentGE");
  assertTokenAtPosition(MetaFLexer.IF_COND_BU_PERCENT_GE, 0, tokens);
});

test('If Condition CellE Tokenization', () => {
  let tokens = setup("CellE");
  assertTokenAtPosition(MetaFLexer.IF_COND_CELL_E, 0, tokens);
});

test('If Condition ChatCapture Tokenization', () => {
  let tokens = setup("ChatCapture");
  assertTokenAtPosition(MetaFLexer.IF_COND_CHAT_CAPTURE, 0, tokens);
});

test('If Condition ChatMatch Tokenization', () => {
  let tokens = setup("ChatMatch");
  assertTokenAtPosition(MetaFLexer.IF_COND_CHAT_MATCH, 0, tokens);
});

test('If Condition Death Tokenization', () => {
  let tokens = setup("Death");
  assertTokenAtPosition(MetaFLexer.IF_COND_DEATH, 0, tokens);
});

test('If Condition DistToRteGE Tokenization', () => {
  let tokens = setup("DistToRteGE");
  assertTokenAtPosition(MetaFLexer.IF_COND_DIST_TO_RTE_GE, 0, tokens);
});

test('If Condition ExitPortal Tokenization', () => {
  let tokens = setup("ExitPortal");
  assertTokenAtPosition(MetaFLexer.IF_COND_EXIT_PORTAL, 0, tokens);
});

test('If Condition Expr Tokenization', () => {
  let tokens = setup("Expr");
  assertTokenAtPosition(MetaFLexer.IF_COND_EXPR, 0, tokens);
});

test('If Condition IntoPortal Tokenization', () => {
  let tokens = setup("IntoPortal");
  assertTokenAtPosition(MetaFLexer.IF_COND_INTO_PORTAL, 0, tokens);
});

test('If Condition ItemCountGE Tokenization', () => {
  let tokens = setup("ItemCountGE");
  assertTokenAtPosition(MetaFLexer.IF_COND_ITEM_COUNT_GE, 0, tokens);
});

test('If Condition ItemCountLE Tokenization', () => {
  let tokens = setup("ItemCountLE");
  assertTokenAtPosition(MetaFLexer.IF_COND_ITEM_COUNT_LE, 0, tokens);
});

test('If Condition MainSlotsLE Tokenization', () => {
  let tokens = setup("MainSlotsLE");
  assertTokenAtPosition(MetaFLexer.IF_COND_MAIN_SLOTS_LE, 0, tokens);
});

test('If Condition MobsInDistName Tokenization', () => {
  let tokens = setup("MobsInDist_Name");
  assertTokenAtPosition(MetaFLexer.IF_COND_MOBS_IN_DIST_NAME, 0, tokens);
});

test('If Condition MobsInDistPriority Tokenization', () => {
  let tokens = setup("MobsInDist_Priority");
  assertTokenAtPosition(MetaFLexer.IF_COND_MOBS_IN_DIST_PRIORITY, 0, tokens);
});

test('If Condition NavEmpty Tokenization', () => {
  let tokens = setup("NavEmpty");
  assertTokenAtPosition(MetaFLexer.IF_COND_NAV_EMPTY, 0, tokens);
});

test('If Condition NeedToBuff Tokenization', () => {
  let tokens = setup("NeedToBuff");
  assertTokenAtPosition(MetaFLexer.IF_COND_NEED_TO_BUFF, 0, tokens);
});

test('If Condition Never Tokenization', () => {
  let tokens = setup("Never");
  assertTokenAtPosition(MetaFLexer.IF_COND_NEVER, 0, tokens);
});

test('If Condition NoMobsInDist Tokenization', () => {
  let tokens = setup("NoMobsInDist");
  assertTokenAtPosition(MetaFLexer.IF_COND_NO_MOBS_IN_DIST, 0, tokens);
});

test('If Condition Not Tokenization', () => {
  let tokens = setup("Not");
  assertTokenAtPosition(MetaFLexer.IF_COND_NOT, 0, tokens);
});

test('If Condition PSecondsInStateGE Tokenization', () => {
  let tokens = setup("PSecsInStateGE");
  assertTokenAtPosition(MetaFLexer.IF_COND_P_SECS_IN_STATE_GE, 0, tokens);
});

test('If Condition SecondsInStateGE Tokenization', () => {
  let tokens = setup("SecsInStateGE");
  assertTokenAtPosition(MetaFLexer.IF_COND_SECS_IN_STATE_GE, 0, tokens);
});

test('If Condition VendorClosed Tokenization', () => {
  let tokens = setup("VendorClosed");
  assertTokenAtPosition(MetaFLexer.IF_COND_VENDOR_CLOSED, 0, tokens);
});

test('If Condition VendorOpen Tokenization', () => {
  let tokens = setup("VendorOpen");
  assertTokenAtPosition(MetaFLexer.IF_COND_VENDOR_OPEN, 0, tokens);
});