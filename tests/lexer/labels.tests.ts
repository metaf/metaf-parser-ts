import { expect, test } from '@jest/globals';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MetaFLexer } from '../../gen/MetaFLexer';
import { assertTokenAtPosition } from '../testhelpers';
import { setup } from './lexer.tests'

test('State Label Tokenization', () => {
  let input = "STATE:";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.STATE_LABEL, 0, tokens);
});

test('If Label Tokenization', () => {
  let input = "IF:";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.IF_LABEL, 0, tokens);
});

test('Do Label Tokenization', () => {
  let input = "DO:";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.DO_LABEL, 0, tokens);
});

test('Nav Label Tokenization', () => {
  let input = "NAV:";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.NAV_LABEL, 0, tokens);
});