import { expect, test } from '@jest/globals';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MetaFLexer } from '../../gen/MetaFLexer';
import { assertTokenAtPosition } from '../testhelpers';

export function setup(inputText: string) : CommonTokenStream {
  const inputStream = CharStreams.fromString(inputText);
  const lexer = new MetaFLexer(inputStream);
  const tokens = new CommonTokenStream(lexer);

  tokens.fill();

  return tokens;
}

test('recognizes identifier tokens', () => {
  let tokens = setup("myVariable123");
  assertTokenAtPosition(MetaFLexer.IDENTIFIER, 0, tokens, "myVariable123");
});

test('recognizes integer tokens', () => {
  let tokens = setup("123");
  assertTokenAtPosition(MetaFLexer.NUMBER, 0, tokens, "123");
});

test('recognizes negative integer tokens', () => {
  let tokens = setup("-456");
  assertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "-456");
});

test('recognizes decimal tokens', () => {
  let tokens = setup("789.012");
  assertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "789.012");
});

test('recognizes negative decimal tokens', () => {
  let tokens = setup("-512.012");
  assertTokenAtPosition(MetaFLexer.DECIMAL, 0, tokens, "-512.012");
});

test('recognizes prefixed hexadecimal tokens', () => {
  let tokens = setup("0x1A3a4234");
  assertTokenAtPosition(MetaFLexer.HEX, 0, tokens, "0x1A3a4234");
});

test('recognizes hexadecimal tokens', () => {
  let tokens = setup("1A3a4234");
  assertTokenAtPosition(MetaFLexer.HEX, 0, tokens, "1A3a4234");
});

test('recognizes block comment tokens', () => {
  let tokens = setup("/~ This is a block comment ~/");
  assertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "/~ This is a block comment ~/");
});

test('recognizes multiline block comment tokens', () => {
  let tokens = setup("/~ This is a block comment\n spanning multiple lines ~/");
  assertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "/~ This is a block comment\n spanning multiple lines ~/");
});

test('recognizes comment line tokens', () => {
  let tokens = setup("~~ Comment at the end of the line");
  assertTokenAtPosition(MetaFLexer.COMMENT, 0, tokens, "~~ Comment at the end of the line");
});

test('recognizes multiple curly strings tokens', () => {
  let tokens = setup("{nextstate}{more}");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "nextstate");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 1, tokens, "more");
});

test('recognizes curly string tokens with spaces', () => {
  let tokens = setup("{test  state}");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test  state");
});

test('recognizes curly string tokens with escaped curlies', () => {
  let tokens = setup("{test\\}state}{\\{}{\\}}");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test}state");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 1, tokens, "{");
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 2, tokens, "}");
});

test('recognizes indentation level', () => {
  let tokens = setup(
`IF:
  IF:
  IF:
IF:
  `);
  assertTokenAtPosition(MetaFLexer.IF_LABEL, 0, tokens);
  assertTokenAtPosition(MetaFLexer.INDENT, 1, tokens);
  assertTokenAtPosition(MetaFLexer.IF_LABEL, 2, tokens);
  assertTokenAtPosition(MetaFLexer.NL, 3, tokens);
  assertTokenAtPosition(MetaFLexer.IF_LABEL, 4, tokens);
  assertTokenAtPosition(MetaFLexer.NL, 5, tokens);
  assertTokenAtPosition(MetaFLexer.DEDENT, 6, tokens);
  assertTokenAtPosition(MetaFLexer.IF_LABEL, 7, tokens);
})

test('recognizes multi line expressions in curlies', () => {
  let tokens = setup("{\nthis[];\nthat[];\n}");
  
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "\nthis[];\nthat[];\n");
})

test('recognizes multi line xml in curlies', () => {
  let tokens = setup("{\n<?xml ?>\n<test>\n</xml>\n}");
  
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "\n<?xml ?>\n<test>\n</xml>\n");
})

test('recognizes double escaped curlies in single line curlies', () => {
  let tokens = setup("{test{{this}}that}");
  
  assertTokenAtPosition(MetaFLexer.CURLYCONTENTS, 0, tokens, "test{this}that");
})