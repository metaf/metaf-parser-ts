import { expect, test } from '@jest/globals';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MetaFLexer } from '../../gen/MetaFLexer';
import { assertTokenAtPosition } from '../testhelpers';
import { setup } from './lexer.tests'

test('Action SetState Tokenization', () => {
  let tokens = setup("SetState");
  assertTokenAtPosition(MetaFLexer.ACTION_SET_STATE, 0, tokens);
});

test('Action Chat Tokenization', () => {
  let tokens = setup("Chat");
  assertTokenAtPosition(MetaFLexer.ACTION_CHAT, 0, tokens);
});

test('Action DoAll Tokenization', () => {
  let tokens = setup("DoAll");
  assertTokenAtPosition(MetaFLexer.ACTION_DO_ALL, 0, tokens);
});

test('Action EmbedNav Tokenization', () => {
  let tokens = setup("EmbedNav");
  assertTokenAtPosition(MetaFLexer.ACTION_EMBED_NAV, 0, tokens);
});

test('Action CallState Tokenization', () => {
  let tokens = setup("CallState");
  assertTokenAtPosition(MetaFLexer.ACTION_CALL_STATE, 0, tokens);
});

test('Action Return Tokenization', () => {
  let tokens = setup("Return");
  assertTokenAtPosition(MetaFLexer.ACTION_RETURN, 0, tokens);
});

test('Action DoExpr Tokenization', () => {
  let tokens = setup("DoExpr");
  assertTokenAtPosition(MetaFLexer.ACTION_DO_EXPR, 0, tokens);
});

test('Action ChatExpr Tokenization', () => {
  let tokens = setup("ChatExpr");
  assertTokenAtPosition(MetaFLexer.ACTION_CHAT_EXPR, 0, tokens);
});

test('Action SetWatchdog Tokenization', () => {
  let tokens = setup("SetWatchdog");
  assertTokenAtPosition(MetaFLexer.ACTION_SET_WATCHDOG, 0, tokens);
});

test('Action ClearWatchdog Tokenization', () => {
  let tokens = setup("ClearWatchdog");
  assertTokenAtPosition(MetaFLexer.ACTION_CLEAR_WATCHDOG, 0, tokens);
});

test('Action GetOpt Tokenization', () => {
  let tokens = setup("GetOpt");
  assertTokenAtPosition(MetaFLexer.ACTION_GET_OPT, 0, tokens);
});

test('Action SetOpt Tokenization', () => {
  let tokens = setup("SetOpt");
  assertTokenAtPosition(MetaFLexer.ACTION_SET_OPT, 0, tokens);
});

test('Action CreateView Tokenization', () => {
  let tokens = setup("CreateView");
  assertTokenAtPosition(MetaFLexer.ACTION_CREATE_VIEW, 0, tokens);
});

test('Action DestroyView Tokenization', () => {
  let tokens = setup("DestroyView");
  assertTokenAtPosition(MetaFLexer.ACTION_DESTROY_VIEW, 0, tokens);
});

test('Action DestroyAllViews Tokenization', () => {
  let tokens = setup("DestroyAllViews");
  assertTokenAtPosition(MetaFLexer.ACTION_DESTROY_ALL_VIEWS, 0, tokens);
});

test('Action None Tokenization', () => {
  let tokens = setup("None");
  assertTokenAtPosition(MetaFLexer.ACTION_NONE, 0, tokens);
});