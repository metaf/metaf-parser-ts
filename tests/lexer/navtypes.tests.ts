import { expect, test } from '@jest/globals';
import { CharStreams, CommonTokenStream } from 'antlr4ts';
import { MetaFLexer } from '../../gen/MetaFLexer';
import { assertTokenAtPosition } from '../testhelpers';
import { setup } from './lexer.tests'

test('Nav Type Circular Tokenization', () => {
  let input = "circular";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.NAV_TYPE_CIRCULAR, 0, tokens);
});

test('Nav Type Linear Tokenization', () => {
  let input = "linear";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.NAV_TYPE_LINEAR, 0, tokens);
});

test('Nav Type Once Tokenization', () => {
  let input = "once";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.NAV_TYPE_ONCE, 0, tokens);
});

test('Nav Type Follow Tokenization', () => {
  let input = "follow";
  let tokens = setup(input);

  assertTokenAtPosition(MetaFLexer.NAV_TYPE_FOLLOW, 0, tokens);
});