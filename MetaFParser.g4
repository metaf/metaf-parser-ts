parser grammar MetaFParser;

options {
    tokenVocab = MetaFLexer;
}

meta: (stateBlock | navBlock | NL)* EOF ;

stateBlock: STATE_LABEL COLON argsList WS* INDENT (ifBlock | NL)* DEDENT NL? ;
ifBlock: IF_LABEL COLON WS+ conditionBlock WS* INDENT NL* doBlock DEDENT NL? ;
doBlock:  DO_LABEL COLON WS+ actionBlock WS* NL? ;
navBlock: NAV_LABEL COLON argsList WS* (INDENT (waypointBlock | NL)* DEDENT)? NL? ;

conditionBlock: (NOT_KEYWORD WS+)* block_id argsList WS* (INDENT (conditionBlock | NL)+ DEDENT)? NL? ;
actionBlock: block_id argsList WS* (INDENT (actionBlock | NL)+ DEDENT)? NL? ;
waypointBlock: block_id argsList WS* NL ;

argsList: (WS+ arg)*;
arg: value=(NUMBER | DECIMAL | HEX | CURLYCONTENTS | IDENTIFIER) ;

block_id: IDENTIFIER;