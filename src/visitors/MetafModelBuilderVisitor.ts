import { AbstractParseTreeVisitor, CommonTokenStream, CharStream, CharStreams, ParserRuleContext, Token, RecognitionException, Vocabulary } from "antlr4ng";
import { MetaFParser, MetaContext, StateBlockContext, IfBlockContext, DoBlockContext, ConditionBlockContext, ActionBlockContext, ArgContext, NavBlockContext, WaypointBlockContext } from "../gen/MetaFParser";
import { MetaFLexer } from "../gen/MetaFlexer";
import { MetafErrorListener, MetafErrorStrategy, MetafErrorType, MetafErrorPosition, MetafError } from "../lib/MetafErrorListener";
import { Meta, StateBlock, IfBlock, DoBlock } from "../models";
import { BlockBase } from "../models/BlockBase";
import { ColonBlockBase } from "../models/ColonBlockBase";
import { ParserModelBase } from "../models/ParserModelBase";
import { DoAllAction, CallStateAction, ChatAction, ChatExprAction, ClearWatchdogAction, CreateViewAction, DestroyAllViewsAction, DestroyViewAction, DoExprAction, EmbedNavAction, GetOptAction, NoneAction, ReturnAction, SetOptAction, SetStateAction, SetWatchdogAction } from "../models/actions";
import ActionBlock from "../models/actions/ActionBlock";
import NavBlock from "../models/blocks/NavBlock";
import { AllCondition, AnyCondition, AlwaysCondition, BlockECondition, BuPercentGECondition, CellECondition, ChatCaptureCondition, ChatMatchCondition, DeathCondition, DistToRteGECondition, ExitPortalCondition, ExprCondition, IntoPortalCondition, ItemCountGECondition, ItemCountLECondition, MainSlotsLECondition, MobsInDist_NameCondition, MobsInDist_PriorityCondition, NavEmptyCondition, NeedToBuffCondition, NeverCondition, NoMobsInDistCondition, PSecsInStateGECondition, SecsInStateGECondition, SecsOnSpellGECondition, VendorClosedCondition, VendorOpenCondition, NotCondition } from "../models/conditions";
import ConditionBlock from "../models/conditions/ConditionBlock";
import { ChatWaypoint, CheckpointWaypoint, FollowWaypoint, JumpWaypoint, OpenVendorWaypoint, PauseWaypoint, PointWaypoint, PortalWaypoint, RecallSpellWaypoint, TalkToNPCWaypoint, UsePortalNPCWaypoint, WaypointBlock } from "../models/navnodes";
import BlockType from "../models/BlockType";

export interface BlockInfo {
  Type: typeof ParserModelBase,
  Name: string,
  Instance: ParserModelBase
}

export default class MetafModelBuilderVisitor extends AbstractParseTreeVisitor<object> {
  public Lexer: MetaFLexer;
  public TokenStream: CommonTokenStream;
  public Parser: MetaFParser;
  public InputStream: CharStream;
  public ErrorListener: MetafErrorListener;
  public ErrorStrategy: MetafErrorStrategy;
  public AFContents: string;
  public Meta: Meta;
  public AFFile: string;

  public static readonly ColonBlockTokens: number[] = [
    MetaFLexer.STATE_LABEL, MetaFLexer.NAV_LABEL, MetaFLexer.IF_LABEL, MetaFLexer.DO_LABEL
  ];

  public static readonly AvailableColonBlocks: typeof ColonBlockBase[] = [
    StateBlock, IfBlock, DoBlock, NavBlock
  ];

  public static readonly AvailableActionBlocks: typeof ActionBlock[] = [
    DoAllAction, CallStateAction, ChatAction, ChatExprAction, ClearWatchdogAction, CreateViewAction, DestroyAllViewsAction, DestroyViewAction,
    DoExprAction, EmbedNavAction, GetOptAction, NoneAction, ReturnAction, SetOptAction, SetStateAction, SetWatchdogAction,
  ];

  public static readonly AvailableConditionBlocks: typeof ConditionBlock[] = [
    AllCondition, AnyCondition, AlwaysCondition, BlockECondition, BuPercentGECondition, CellECondition, ChatCaptureCondition, ChatMatchCondition,
    DeathCondition, DistToRteGECondition, ExitPortalCondition, ExprCondition, IntoPortalCondition, ItemCountGECondition,
    ItemCountLECondition, MainSlotsLECondition, MobsInDist_NameCondition, MobsInDist_PriorityCondition, NavEmptyCondition,
    NeedToBuffCondition, NeverCondition, NoMobsInDistCondition, PSecsInStateGECondition, SecsInStateGECondition,
    SecsOnSpellGECondition, VendorClosedCondition, VendorOpenCondition, NotCondition,
  ];

  public static readonly AvailableWaypointBlocks: typeof WaypointBlock[] = [
    ChatWaypoint, CheckpointWaypoint, FollowWaypoint, JumpWaypoint, OpenVendorWaypoint, PauseWaypoint, PointWaypoint, PortalWaypoint,
    RecallSpellWaypoint, TalkToNPCWaypoint, UsePortalNPCWaypoint
  ];

  public static readonly AvailableBlocks: typeof BlockBase[] = [
    ...MetafModelBuilderVisitor.AvailableActionBlocks,
    ...MetafModelBuilderVisitor.AvailableConditionBlocks,
    ...MetafModelBuilderVisitor.AvailableWaypointBlocks
  ];

  ValidArgumentTokenTypes: number[] = [
    MetaFLexer.NUMBER,
    MetaFLexer.DECIMAL,
    MetaFLexer.CURLYCONTENTS,
    MetaFLexer.HEX,
    MetaFLexer.IDENTIFIER
  ];

  private static hasCache = false;
  private static colonBlockLookup: Record<string, BlockInfo>;
  public static blockLookup: Record<string, BlockInfo>;
  private static classIdentifierLookup: Record<string, string>;
  private tokenIndexToModelLookup: Record<string, ParserModelInfo>;

  constructor(filename: string, contents: string) {
    super();
    this.AFFile = filename;
    this.AFContents = contents;

    this.InputStream = CharStreams.fromString(this.AFContents);
    this.Lexer = new MetaFLexer(this.InputStream);
    this.TokenStream = new CommonTokenStream(this.Lexer);
    this.Parser = new MetaFParser(this.TokenStream);

    this.Lexer.removeErrorListeners();
    this.Parser.removeErrorListeners();

    this.ErrorListener = new MetafErrorListener(this);
    this.ErrorStrategy = new MetafErrorStrategy(this);
    
    this.Lexer.addErrorListener(this.ErrorListener);
    this.Parser.addErrorListener(this.ErrorListener);
    this.Parser.errorHandler = this.ErrorStrategy;

    this.buildCache();
  }

  private buildCache() {
    if (!MetafModelBuilderVisitor.hasCache) {
      MetafModelBuilderVisitor.classIdentifierLookup = {};

      MetafModelBuilderVisitor.colonBlockLookup = {};
      for (const t of MetafModelBuilderVisitor.AvailableColonBlocks) {
        const instance = new (t as any)(null) as ColonBlockBase;
        const conditionInfo = {
          Type: t as any,
          Name: instance.IdentifierName,
          Instance: instance
        } as BlockInfo
        MetafModelBuilderVisitor.classIdentifierLookup[t.name] = instance.IdentifierName;
        MetafModelBuilderVisitor.colonBlockLookup[instance.IdentifierName] = conditionInfo;
      }

      MetafModelBuilderVisitor.blockLookup = {};
      for (const t of MetafModelBuilderVisitor.AvailableBlocks) {
        const instance = new (t as any)(null) as BlockBase;
        const actionInfo = {
          Type: t as any,
          Name: instance.IdentifierName,
          Instance: instance
        } as BlockInfo
        MetafModelBuilderVisitor.classIdentifierLookup[t.name] = instance.IdentifierName;
        MetafModelBuilderVisitor.blockLookup[instance.IdentifierName] = actionInfo;
      }

      MetafModelBuilderVisitor.hasCache = true;
    }
  }

  build(): Meta | null {
    this.tokenIndexToModelLookup = {}

    this.Meta = this.visitMeta(this.Parser.meta());

    //this.Meta.printTree()

    return this.Meta;
  }

  visitMeta = (ctx: MetaContext): Meta => {
    let meta = new Meta(ctx, undefined);
    const blockInfo = MetafModelBuilderVisitor.colonBlockLookup[meta.IdentifierName];
    this.cacheModelContext(ctx, {
      Model: meta,
      BlockType: BlockType.Meta,
      BlockInfo: blockInfo
    });

    for (const stateBlock of ctx.stateBlock()) {
      const state = this.visitStateBlock(stateBlock, meta);
      if (state && state instanceof StateBlock) {
        meta.States.push(state);
      }
    }

    for (const navBlock of ctx.navBlock()) {
      const nav = this.visitNavBlock(navBlock, meta);
      if (nav && nav instanceof NavBlock) {
        meta.NavRoutes.push(nav);
      }
    }

    return meta;
  }

  visitNavBlock(ctx: NavBlockContext, meta: Meta) {
    const nav = new NavBlock(ctx, meta);
    const blockInfo = MetafModelBuilderVisitor.colonBlockLookup[nav.IdentifierName];
    this.cacheModelContext(ctx, {
      Model: nav,
      BlockType: BlockType.Nav,
      BlockInfo: blockInfo
    });

    this.visitAndPopulateArgsList(ctx.argsList()?.arg(), ctx, blockInfo, nav);

    for (const child of ctx.waypointBlock() || []) {
      const childBlock = this.visitNavNodeBlock(child, nav);
      if (childBlock) {
        nav.Waypoints.push(childBlock);
      }
    }

    return nav;
  }
  
  visitNavNodeBlock(ctx: WaypointBlockContext, nav: NavBlock) : WaypointBlock | undefined {
    if (!ctx)
      return;
    
    const blockName = ctx.block_id()?.getText();
    const blockInfo = MetafModelBuilderVisitor.blockLookup[blockName];

    let block: WaypointBlock | undefined;

    if (blockInfo) {
      block = this.makeNew(blockInfo, ctx, nav) as WaypointBlock;
      if (!MetafModelBuilderVisitor.AvailableWaypointBlocks.includes(blockInfo.Type as typeof WaypointBlock)) {
        this.reportError(ctx, MetafErrorType.InvalidBlock, `Block '${blockInfo.Name}' is not an Waypoint block!`);
        return;
      }
      this.cacheModelContext(ctx, {
        Model: block,
        BlockType: BlockType.Waypoint,
        BlockInfo: blockInfo
      });
      this.visitAndPopulateArgsList(ctx.argsList()?.arg(), ctx, blockInfo, block);
    }
    else {
      this.reportError(ctx, MetafErrorType.UnexpectedIdentifier, `Unknown block type: ${blockName}`);
    }

    return block;
  }

  visitStateBlock(ctx: StateBlockContext, meta: Meta): StateBlock | undefined {
    const state = new StateBlock(ctx, meta);
    const blockInfo = MetafModelBuilderVisitor.colonBlockLookup[state.IdentifierName];
    this.cacheModelContext(ctx, {
      Model: state,
      BlockType: BlockType.State,
      BlockInfo: blockInfo
    });

    this.visitAndPopulateArgsList(ctx.argsList()?.arg(), ctx, blockInfo, state);

    for (const child of ctx.ifBlock() || []) {
      const childBlock = this.visitIfBlock(child, state);
      if (childBlock) {
        state.Rules.push(childBlock);
      }
    }

    return state;
  }

  visitIfBlock(ctx: IfBlockContext, state: StateBlock) : IfBlock | undefined {
    if (!ctx)
      return;
    
    const ifBlock = new IfBlock(ctx, state);
    const blockInfo = MetafModelBuilderVisitor.colonBlockLookup[ifBlock.IdentifierName];
    this.cacheModelContext(ctx, {
      Model: ifBlock,
      BlockType: BlockType.If,
      BlockInfo: blockInfo
    });

    ifBlock.Condition = this.visitConditionBlock(ctx.conditionBlock(), ifBlock);

    if (ctx.doBlock()) {
      ifBlock.Do = this.visitDoBlock(ctx.doBlock(), ifBlock);
    }

    return ifBlock;
  }

  visitDoBlock(ctx: DoBlockContext, ifBlock: IfBlock): DoBlock | undefined {
    if (!ctx)
      return;

    const doBlock = new DoBlock(ctx, ifBlock);
    const blockInfo = MetafModelBuilderVisitor.colonBlockLookup[doBlock.IdentifierName];
    this.cacheModelContext(ctx, {
      Model: doBlock,
      BlockType: BlockType.Do,
      BlockInfo: blockInfo
    });

    doBlock.Action = this.visitActionBlock(ctx.actionBlock(), doBlock);

    return doBlock;
  }

  visitConditionBlock(ctx: ConditionBlockContext, parentModel: ParserModelBase) : ConditionBlock | undefined {
    if (!ctx)
      return;
    
    const blockName = ctx.block_id()?.getText();
    const blockInfo = MetafModelBuilderVisitor.blockLookup[blockName];

    let block: ConditionBlock | undefined;

    if (blockInfo) {
      block = this.makeNew(blockInfo, ctx, parentModel) as ConditionBlock;
      if (!MetafModelBuilderVisitor.AvailableConditionBlocks.includes(blockInfo.Type as typeof ConditionBlock)) {
        this.reportError(ctx, MetafErrorType.InvalidBlock, `Block '${blockInfo.Name}' is not an Condition block!`);
        return;
      }
      this.cacheModelContext(ctx, {
        Model: block,
        BlockType: BlockType.Condition,
        BlockInfo: blockInfo
      });
      this.visitAndPopulateArgsList(ctx.argsList()?.arg(), ctx, blockInfo, block);

      if (ctx.NOT_KEYWORD() && ctx.NOT_KEYWORD().length > 0 && block instanceof ConditionBlock) {
        block.IsNotPrefixed = ctx.NOT_KEYWORD().length % 2 != 0;
      }

      if (block.ValidChildren.length == 0 && (ctx.conditionBlock() || []).length > 0) {
        this.reportError(ctx, MetafErrorType.ChildrenNotSupported, `Block type ${block.IdentifierName} does not support child blocks!`);
      }
      else {
        for (const child of ctx.conditionBlock() || []) {
          const childBlock = this.visitConditionBlock(child, block);

          if (!block.ValidChildren.includes(blockInfo.Type)) {
            const allowedChildren = block.ValidChildren.map(c => MetafModelBuilderVisitor.classIdentifierLookup[c.name] || c.name).join(" | ")
            this.reportError(ctx, MetafErrorType.InvalidChild, `Invalid child block '${blockName}'. Block type '${block.IdentifierName}' only supports the following child block types: ${allowedChildren}`);
          }
          else if (childBlock) {
            block.Children.push(childBlock);
          }
        }
      }
    }
    else {
      this.reportError(ctx, MetafErrorType.UnexpectedIdentifier, `Unknown block type: ${blockName}`);
    }

    return block;
  }

  visitActionBlock(ctx: ActionBlockContext, parentModel: ParserModelBase) : ActionBlock | undefined {
    if (!ctx)
      return;
    
    const blockName = ctx.block_id()?.getText();
    const blockInfo = MetafModelBuilderVisitor.blockLookup[blockName];

    let block: ActionBlock | undefined;

    if (blockInfo) {
      block = this.makeNew(blockInfo, ctx, parentModel) as ActionBlock;
      if (!MetafModelBuilderVisitor.AvailableActionBlocks.includes(blockInfo.Type)) {
        this.reportError(ctx, MetafErrorType.InvalidBlock, `Block '${blockInfo.Name}' is not an Action block!`);
        return;
      }
      this.cacheModelContext(ctx, {
        Model: block,
        BlockType: BlockType.Action,
        BlockInfo: blockInfo
      });
      this.visitAndPopulateArgsList(ctx.argsList()?.arg(), ctx, blockInfo, block);

      if (block.ValidChildren.length == 0 && (ctx.actionBlock() || []).length > 0) {
        this.reportError(ctx, MetafErrorType.ChildrenNotSupported, `Block type ${block.IdentifierName} does not support child blocks!`);
      }
      else {
        for (const child of ctx.actionBlock() || []) {
          const childBlock = this.visitActionBlock(child, block);

          if (!block.ValidChildren.includes(blockInfo.Type)) {
            const allowedChildren = block.ValidChildren.map(c => MetafModelBuilderVisitor.classIdentifierLookup[c.name] || c.name).join(" | ")
            this.reportError(ctx, MetafErrorType.InvalidChild, `Invalid child block '${blockName}'. Block type '${block.IdentifierName}' only supports the following child block types: ${allowedChildren}`);
          }
          else if (childBlock) {
            block.Children.push(childBlock);
          }
        }
      }
    }
    else {
      this.reportError(ctx, MetafErrorType.UnexpectedIdentifier, `Unknown block type: ${blockName}`);
    }

    return block;
  }

  private visitAndPopulateArgsList(args: ArgContext[], parentCtx: ParserRuleContext, blockInfo: BlockInfo, model: ParserModelBase) {
    if (!args) {
      return;
    }

    const minArgCount = blockInfo.Instance.Arguments.filter(a => a.DefaultValue === undefined).length;
    const maxArgCount = blockInfo.Instance.Arguments.length;

    // check each argument type / assign defaults
    let usedArgumentCount = 0;
    for (let i = 0; i < blockInfo.Instance.Arguments.length; i++) {
      const argInfo = blockInfo.Instance.Arguments[i];
      if (args.length > i) {
        const tokenType = args[i]._value.type;
        if (argInfo.ValidTokenTypes.indexOf(tokenType) === -1) {
          this.reportError(args[i], MetafErrorType.InvalidArgumentType, `${blockInfo.Name} argument #${i+1} expected a ${formatTokens(this.Parser.vocabulary, argInfo.ValidTokenTypes)} but a ${this.Parser.vocabulary.getDisplayName(tokenType)} was passed instead`);
        }

        // populate model with passed arg value
        if (argInfo.ValueResolver) {
          model[argInfo.Name] = argInfo.ValueResolver(args[i]._value, args[i], this, model, args, usedArgumentCount)
        }
        else {
          model[argInfo.Name] = this.visitArg(args[i], args[i]._value)
        }

        this.cacheModelContext(args[i], {
          Model: model,
          BlockType: BlockType.Argument,
          ArgumentIndex: i
        });
        
        if (model[argInfo.Name] instanceof ParserModelBase) {
          // arg was resolved to a child_block
          const allowedChildren = (argInfo.ValidChildBlocks || []).map(c => MetafModelBuilderVisitor.classIdentifierLookup[c.name] || c.name)
          const childBlockName = (model[argInfo.Name] as ParserModelBase).IdentifierName;
          if (!allowedChildren.includes(childBlockName)) {
            this.reportError(args[i], MetafErrorType.InvalidArgumentType, `${blockInfo.Name} argument #${i+1} '${childBlockName}'. Expected one of: ${allowedChildren.join(' | ')}`);
          }
        }
      }
      // fill in default
      else {
        model[argInfo.Name] = argInfo.DefaultValue;
      }
    }

    // check overall argument count
    if (args.length < minArgCount || args.length > maxArgCount) {
      const argCount = (minArgCount == maxArgCount ? minArgCount.toString() : `${minArgCount}-${maxArgCount}`);
      const gotArgsDisplay = args.map(a => (a._value?.text ? this.Parser.vocabulary.getSymbolicName(a._value.type) : 'child block')).join(',');
      this.reportError(parentCtx, MetafErrorType.InvalidArgumentCount, `Invalid argument count. ${blockInfo.Name} expects ${argCount} arguments but ${args.length} were passed! Got: ${gotArgsDisplay}`);
    }
  }

  visitArg(ctx: ParserRuleContext, token: Token): number | string {
    switch(token.type){
      case MetaFLexer.NUMBER:
        try {
          return parseInt(token.text)
        }
        catch(ex) {
          this.reportError(ctx, MetafErrorType.ParseError, `Failed to parse number '${token.text}': ${ex.message}`);
          return 0;
        }

      case MetaFLexer.DECIMAL:
        try {
          return parseFloat(token.text)
        }
        catch(ex) {
          this.reportError(ctx, MetafErrorType.ParseError, `Failed to parse decimal '${token.text}': ${ex.message}`);
          return 0;
        }

      case MetaFLexer.HEX:
        try {
          return parseInt(token.text, 16);
        }
        catch(ex) {
          this.reportError(ctx, MetafErrorType.ParseError, `Failed to parse hex '${token.text}': ${ex.message}`);
          return 0;
        }
      
      default:
        return token.text;
    }
  }

  public reportError(ctx: ParserRuleContext, type: MetafErrorType, message: string, ex: RecognitionException | undefined=undefined) {
    this.ErrorStrategy.reportMetafError({
      Message: message,
      Exception: ex,
      RuleIndex: ctx.ruleIndex,
      Context: ctx,
      Type: type,
      Start: {
        TokenIndex: ctx.start.tokenIndex,
        Line: ctx.start.line,
        Column: ctx.start.column,
      } as MetafErrorPosition,
      End: {
        TokenIndex: ctx.stop.tokenIndex,
        Line: ctx.stop.line,
        Column: ctx.stop.column,
      } as MetafErrorPosition
    } as MetafError);
  }

  private makeNew(blockInfo: BlockInfo, ctx: ParserRuleContext, parent: ParserModelBase | undefined): ParserModelBase {
    const instance = new (blockInfo.Type as any)(ctx, parent);

    if (instance instanceof ParserModelBase && instance.Deprecated) {
      this.reportError(ctx, MetafErrorType.Deprecated, instance.DeprecatedMessage);
    }

    return instance;
  }

  private cacheModelContext(ctx: ParserRuleContext, modelInfo: ParserModelInfo, parentCtx: ParserRuleContext | null=null) {
    const newModelInfo = {
      ...modelInfo,
      Context: parentCtx ?? ctx
    };

    if (typeof(newModelInfo.ArgumentIndex) == 'number') {
      modelInfo.Model.ArgumentContexts[newModelInfo.ArgumentIndex] = ctx;
      modelInfo.Model.Arguments[newModelInfo.ArgumentIndex].Context = ctx;
    }

    for (const t of ctx.children || []) {
      const payload = t.getPayload();
      if (payload instanceof ParserRuleContext) {
        this.cacheModelContext(payload, newModelInfo, parentCtx ?? ctx);
      }
      else {
        this.tokenIndexToModelLookup[(payload as Token).tokenIndex] = newModelInfo;
      }
    }
  }

  public getTokenModelInfo(token: Token | undefined) : ParserModelInfo | undefined {
    if (!token)
      return;
    return this.tokenIndexToModelLookup[token.tokenIndex];
  }
}

export interface ParserModelInfo {
  Model: ParserModelBase;
  BlockType: BlockType;
  BlockInfo?: BlockInfo | undefined;
  Context?: ParserRuleContext | undefined;
  ArgumentIndex?: number | undefined;
}

function getRuleText(ctx: ParserRuleContext | undefined): string | undefined {
  return ctx?.getText()?.trim() ?? undefined;
}

function formatTokens(vocabulary: Vocabulary, tokens: number[]): string {
  return tokens.map(t => vocabulary.getDisplayName(t)).join(' | ');
}
