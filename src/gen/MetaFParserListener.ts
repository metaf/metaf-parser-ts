// Generated from ./MetaFParser.g4 by ANTLR 4.13.1

import { ErrorNode, ParseTreeListener, ParserRuleContext, TerminalNode } from "antlr4ng";


import { MetaContext } from "./MetaFParser.js";
import { StateBlockContext } from "./MetaFParser.js";
import { IfBlockContext } from "./MetaFParser.js";
import { DoBlockContext } from "./MetaFParser.js";
import { NavBlockContext } from "./MetaFParser.js";
import { ConditionBlockContext } from "./MetaFParser.js";
import { ActionBlockContext } from "./MetaFParser.js";
import { WaypointBlockContext } from "./MetaFParser.js";
import { ArgsListContext } from "./MetaFParser.js";
import { ArgContext } from "./MetaFParser.js";
import { Block_idContext } from "./MetaFParser.js";


/**
 * This interface defines a complete listener for a parse tree produced by
 * `MetaFParser`.
 */
export class MetaFParserListener implements ParseTreeListener {
    /**
     * Enter a parse tree produced by `MetaFParser.meta`.
     * @param ctx the parse tree
     */
    enterMeta?: (ctx: MetaContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.meta`.
     * @param ctx the parse tree
     */
    exitMeta?: (ctx: MetaContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.stateBlock`.
     * @param ctx the parse tree
     */
    enterStateBlock?: (ctx: StateBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.stateBlock`.
     * @param ctx the parse tree
     */
    exitStateBlock?: (ctx: StateBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.ifBlock`.
     * @param ctx the parse tree
     */
    enterIfBlock?: (ctx: IfBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.ifBlock`.
     * @param ctx the parse tree
     */
    exitIfBlock?: (ctx: IfBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.doBlock`.
     * @param ctx the parse tree
     */
    enterDoBlock?: (ctx: DoBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.doBlock`.
     * @param ctx the parse tree
     */
    exitDoBlock?: (ctx: DoBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.navBlock`.
     * @param ctx the parse tree
     */
    enterNavBlock?: (ctx: NavBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.navBlock`.
     * @param ctx the parse tree
     */
    exitNavBlock?: (ctx: NavBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.conditionBlock`.
     * @param ctx the parse tree
     */
    enterConditionBlock?: (ctx: ConditionBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.conditionBlock`.
     * @param ctx the parse tree
     */
    exitConditionBlock?: (ctx: ConditionBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.actionBlock`.
     * @param ctx the parse tree
     */
    enterActionBlock?: (ctx: ActionBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.actionBlock`.
     * @param ctx the parse tree
     */
    exitActionBlock?: (ctx: ActionBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.waypointBlock`.
     * @param ctx the parse tree
     */
    enterWaypointBlock?: (ctx: WaypointBlockContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.waypointBlock`.
     * @param ctx the parse tree
     */
    exitWaypointBlock?: (ctx: WaypointBlockContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.argsList`.
     * @param ctx the parse tree
     */
    enterArgsList?: (ctx: ArgsListContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.argsList`.
     * @param ctx the parse tree
     */
    exitArgsList?: (ctx: ArgsListContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.arg`.
     * @param ctx the parse tree
     */
    enterArg?: (ctx: ArgContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.arg`.
     * @param ctx the parse tree
     */
    exitArg?: (ctx: ArgContext) => void;
    /**
     * Enter a parse tree produced by `MetaFParser.block_id`.
     * @param ctx the parse tree
     */
    enterBlock_id?: (ctx: Block_idContext) => void;
    /**
     * Exit a parse tree produced by `MetaFParser.block_id`.
     * @param ctx the parse tree
     */
    exitBlock_id?: (ctx: Block_idContext) => void;

    visitTerminal(node: TerminalNode): void {}
    visitErrorNode(node: ErrorNode): void {}
    enterEveryRule(node: ParserRuleContext): void {}
    exitEveryRule(node: ParserRuleContext): void {}
}

