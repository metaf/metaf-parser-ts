import { MetaFLexerBase } from '../lib/MetaFLexerBase';
// Generated from ./MetaFLexer.g4 by ANTLR 4.13.1

import * as antlr from "antlr4ng";
import { Token } from "antlr4ng";


export class MetaFLexer extends MetaFLexerBase {
    public static readonly INDENT = 1;
    public static readonly DEDENT = 2;
    public static readonly CURLYCONTENTS = 3;
    public static readonly NOT_KEYWORD = 4;
    public static readonly STATE_LABEL = 5;
    public static readonly NAV_LABEL = 6;
    public static readonly IF_LABEL = 7;
    public static readonly DO_LABEL = 8;
    public static readonly COLON = 9;
    public static readonly OPENCURL = 10;
    public static readonly CLOSECURL = 11;
    public static readonly PLUS = 12;
    public static readonly MINUS = 13;
    public static readonly HEX = 14;
    public static readonly NUMBER = 15;
    public static readonly DECIMAL = 16;
    public static readonly IDENTIFIER = 17;
    public static readonly COMMENT = 18;
    public static readonly NL = 19;
    public static readonly WS = 20;
    public static readonly ANY = 21;

    public static readonly channelNames = [
        "DEFAULT_TOKEN_CHANNEL", "HIDDEN"
    ];

    public static readonly literalNames = [
        null, null, null, null, "'Not'", "'STATE'", "'NAV'", "'IF'", "'DO'", 
        "':'", "'{'", "'}'", "'+'", "'-'"
    ];

    public static readonly symbolicNames = [
        null, "INDENT", "DEDENT", "CURLYCONTENTS", "NOT_KEYWORD", "STATE_LABEL", 
        "NAV_LABEL", "IF_LABEL", "DO_LABEL", "COLON", "OPENCURL", "CLOSECURL", 
        "PLUS", "MINUS", "HEX", "NUMBER", "DECIMAL", "IDENTIFIER", "COMMENT", 
        "NL", "WS", "ANY"
    ];

    public static readonly modeNames = [
        "DEFAULT_MODE",
    ];

    public static readonly ruleNames = [
        "INDENTATION", "HCHAR", "DIGIT", "CHAR", "NOT_KEYWORD", "STATE_LABEL", 
        "NAV_LABEL", "IF_LABEL", "DO_LABEL", "COLON", "OPENCURL", "CLOSECURL", 
        "PLUS", "MINUS", "HEX", "NUMBER", "DECIMAL", "IDENTIFIER", "COMMENT", 
        "NL", "WS", "ANY",
    ];


    public constructor(input: antlr.CharStream) {
        super(input);
        this.interpreter = new antlr.LexerATNSimulator(this, MetaFLexer._ATN, MetaFLexer.decisionsToDFA, new antlr.PredictionContextCache());
    }

    public get grammarFileName(): string { return "MetaFLexer.g4"; }

    public get literalNames(): (string | null)[] { return MetaFLexer.literalNames; }
    public get symbolicNames(): (string | null)[] { return MetaFLexer.symbolicNames; }
    public get ruleNames(): string[] { return MetaFLexer.ruleNames; }

    public get serializedATN(): number[] { return MetaFLexer._serializedATN; }

    public get channelNames(): string[] { return MetaFLexer.channelNames; }

    public get modeNames(): string[] { return MetaFLexer.modeNames; }

    public static readonly _serializedATN: number[] = [
        4,0,21,186,6,-1,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,
        2,6,7,6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,2,11,7,11,2,12,7,12,2,
        13,7,13,2,14,7,14,2,15,7,15,2,16,7,16,2,17,7,17,2,18,7,18,2,19,7,
        19,2,20,7,20,2,21,7,21,1,0,5,0,47,8,0,10,0,12,0,50,9,0,1,1,1,1,1,
        2,1,2,1,3,3,3,57,8,3,1,4,1,4,1,4,1,4,1,5,1,5,1,5,1,5,1,5,1,5,1,6,
        1,6,1,6,1,6,1,7,1,7,1,7,1,8,1,8,1,8,1,9,1,9,1,10,1,10,1,11,1,11,
        1,12,1,12,1,13,1,13,1,14,1,14,3,14,91,8,14,1,14,1,14,1,14,1,14,1,
        14,1,14,1,14,1,14,1,14,1,15,4,15,103,8,15,11,15,12,15,104,1,16,3,
        16,108,8,16,1,16,4,16,111,8,16,11,16,12,16,112,1,16,1,16,4,16,117,
        8,16,11,16,12,16,118,3,16,121,8,16,1,16,1,16,1,16,4,16,126,8,16,
        11,16,12,16,127,3,16,130,8,16,1,17,1,17,1,17,1,17,5,17,136,8,17,
        10,17,12,17,139,9,17,1,18,3,18,142,8,18,1,18,5,18,145,8,18,10,18,
        12,18,148,9,18,1,18,1,18,1,18,1,18,5,18,154,8,18,10,18,12,18,157,
        9,18,1,18,1,18,1,18,1,18,5,18,163,8,18,10,18,12,18,166,9,18,1,18,
        1,18,3,18,170,8,18,1,18,1,18,1,19,3,19,175,8,19,1,19,1,19,1,19,1,
        20,4,20,181,8,20,11,20,12,20,182,1,21,1,21,1,164,0,22,1,0,3,0,5,
        0,7,0,9,4,11,5,13,6,15,7,17,8,19,9,21,10,23,11,25,12,27,13,29,14,
        31,15,33,16,35,17,37,18,39,19,41,20,43,21,1,0,6,2,0,9,9,32,32,3,
        0,48,57,65,70,97,102,1,0,48,57,3,0,65,90,95,95,97,122,2,0,43,43,
        45,45,1,0,10,10,200,0,9,1,0,0,0,0,11,1,0,0,0,0,13,1,0,0,0,0,15,1,
        0,0,0,0,17,1,0,0,0,0,19,1,0,0,0,0,21,1,0,0,0,0,23,1,0,0,0,0,25,1,
        0,0,0,0,27,1,0,0,0,0,29,1,0,0,0,0,31,1,0,0,0,0,33,1,0,0,0,0,35,1,
        0,0,0,0,37,1,0,0,0,0,39,1,0,0,0,0,41,1,0,0,0,0,43,1,0,0,0,1,48,1,
        0,0,0,3,51,1,0,0,0,5,53,1,0,0,0,7,56,1,0,0,0,9,58,1,0,0,0,11,62,
        1,0,0,0,13,68,1,0,0,0,15,72,1,0,0,0,17,75,1,0,0,0,19,78,1,0,0,0,
        21,80,1,0,0,0,23,82,1,0,0,0,25,84,1,0,0,0,27,86,1,0,0,0,29,90,1,
        0,0,0,31,102,1,0,0,0,33,107,1,0,0,0,35,131,1,0,0,0,37,169,1,0,0,
        0,39,174,1,0,0,0,41,180,1,0,0,0,43,184,1,0,0,0,45,47,7,0,0,0,46,
        45,1,0,0,0,47,50,1,0,0,0,48,46,1,0,0,0,48,49,1,0,0,0,49,2,1,0,0,
        0,50,48,1,0,0,0,51,52,7,1,0,0,52,4,1,0,0,0,53,54,7,2,0,0,54,6,1,
        0,0,0,55,57,7,3,0,0,56,55,1,0,0,0,57,8,1,0,0,0,58,59,5,78,0,0,59,
        60,5,111,0,0,60,61,5,116,0,0,61,10,1,0,0,0,62,63,5,83,0,0,63,64,
        5,84,0,0,64,65,5,65,0,0,65,66,5,84,0,0,66,67,5,69,0,0,67,12,1,0,
        0,0,68,69,5,78,0,0,69,70,5,65,0,0,70,71,5,86,0,0,71,14,1,0,0,0,72,
        73,5,73,0,0,73,74,5,70,0,0,74,16,1,0,0,0,75,76,5,68,0,0,76,77,5,
        79,0,0,77,18,1,0,0,0,78,79,5,58,0,0,79,20,1,0,0,0,80,81,5,123,0,
        0,81,22,1,0,0,0,82,83,5,125,0,0,83,24,1,0,0,0,84,85,5,43,0,0,85,
        26,1,0,0,0,86,87,5,45,0,0,87,28,1,0,0,0,88,89,5,48,0,0,89,91,5,120,
        0,0,90,88,1,0,0,0,90,91,1,0,0,0,91,92,1,0,0,0,92,93,3,3,1,0,93,94,
        3,3,1,0,94,95,3,3,1,0,95,96,3,3,1,0,96,97,3,3,1,0,97,98,3,3,1,0,
        98,99,3,3,1,0,99,100,3,3,1,0,100,30,1,0,0,0,101,103,3,5,2,0,102,
        101,1,0,0,0,103,104,1,0,0,0,104,102,1,0,0,0,104,105,1,0,0,0,105,
        32,1,0,0,0,106,108,3,27,13,0,107,106,1,0,0,0,107,108,1,0,0,0,108,
        110,1,0,0,0,109,111,3,5,2,0,110,109,1,0,0,0,111,112,1,0,0,0,112,
        110,1,0,0,0,112,113,1,0,0,0,113,120,1,0,0,0,114,116,5,46,0,0,115,
        117,3,5,2,0,116,115,1,0,0,0,117,118,1,0,0,0,118,116,1,0,0,0,118,
        119,1,0,0,0,119,121,1,0,0,0,120,114,1,0,0,0,120,121,1,0,0,0,121,
        129,1,0,0,0,122,123,5,69,0,0,123,125,7,4,0,0,124,126,7,2,0,0,125,
        124,1,0,0,0,126,127,1,0,0,0,127,125,1,0,0,0,127,128,1,0,0,0,128,
        130,1,0,0,0,129,122,1,0,0,0,129,130,1,0,0,0,130,34,1,0,0,0,131,137,
        3,7,3,0,132,136,3,7,3,0,133,136,3,5,2,0,134,136,5,95,0,0,135,132,
        1,0,0,0,135,133,1,0,0,0,135,134,1,0,0,0,136,139,1,0,0,0,137,135,
        1,0,0,0,137,138,1,0,0,0,138,36,1,0,0,0,139,137,1,0,0,0,140,142,3,
        39,19,0,141,140,1,0,0,0,141,142,1,0,0,0,142,146,1,0,0,0,143,145,
        3,41,20,0,144,143,1,0,0,0,145,148,1,0,0,0,146,144,1,0,0,0,146,147,
        1,0,0,0,147,149,1,0,0,0,148,146,1,0,0,0,149,150,5,126,0,0,150,151,
        5,126,0,0,151,155,1,0,0,0,152,154,8,5,0,0,153,152,1,0,0,0,154,157,
        1,0,0,0,155,153,1,0,0,0,155,156,1,0,0,0,156,170,1,0,0,0,157,155,
        1,0,0,0,158,159,5,47,0,0,159,160,5,126,0,0,160,164,1,0,0,0,161,163,
        9,0,0,0,162,161,1,0,0,0,163,166,1,0,0,0,164,165,1,0,0,0,164,162,
        1,0,0,0,165,167,1,0,0,0,166,164,1,0,0,0,167,168,5,126,0,0,168,170,
        5,47,0,0,169,141,1,0,0,0,169,158,1,0,0,0,170,171,1,0,0,0,171,172,
        6,18,0,0,172,38,1,0,0,0,173,175,5,13,0,0,174,173,1,0,0,0,174,175,
        1,0,0,0,175,176,1,0,0,0,176,177,5,10,0,0,177,178,3,1,0,0,178,40,
        1,0,0,0,179,181,7,0,0,0,180,179,1,0,0,0,181,182,1,0,0,0,182,180,
        1,0,0,0,182,183,1,0,0,0,183,42,1,0,0,0,184,185,9,0,0,0,185,44,1,
        0,0,0,20,0,48,56,90,104,107,112,118,120,127,129,135,137,141,146,
        155,164,169,174,182,1,0,1,0
    ];

    private static __ATN: antlr.ATN;
    public static get _ATN(): antlr.ATN {
        if (!MetaFLexer.__ATN) {
            MetaFLexer.__ATN = new antlr.ATNDeserializer().deserialize(MetaFLexer._serializedATN);
        }

        return MetaFLexer.__ATN;
    }


    private static readonly vocabulary = new antlr.Vocabulary(MetaFLexer.literalNames, MetaFLexer.symbolicNames, []);

    public override get vocabulary(): antlr.Vocabulary {
        return MetaFLexer.vocabulary;
    }

    private static readonly decisionsToDFA = MetaFLexer._ATN.decisionToState.map( (ds: antlr.DecisionState, index: number) => new antlr.DFA(ds, index) );
}