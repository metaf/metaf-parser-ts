// Generated from ./MetaFParser.g4 by ANTLR 4.13.1

import { AbstractParseTreeVisitor } from "antlr4ng";


import { MetaContext } from "./MetaFParser.js";
import { StateBlockContext } from "./MetaFParser.js";
import { IfBlockContext } from "./MetaFParser.js";
import { DoBlockContext } from "./MetaFParser.js";
import { NavBlockContext } from "./MetaFParser.js";
import { ConditionBlockContext } from "./MetaFParser.js";
import { ActionBlockContext } from "./MetaFParser.js";
import { WaypointBlockContext } from "./MetaFParser.js";
import { ArgsListContext } from "./MetaFParser.js";
import { ArgContext } from "./MetaFParser.js";
import { Block_idContext } from "./MetaFParser.js";


/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by `MetaFParser`.
 *
 * @param <Result> The return type of the visit operation. Use `void` for
 * operations with no return type.
 */
export class MetaFParserVisitor<Result> extends AbstractParseTreeVisitor<Result> {
    /**
     * Visit a parse tree produced by `MetaFParser.meta`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitMeta?: (ctx: MetaContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.stateBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitStateBlock?: (ctx: StateBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.ifBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitIfBlock?: (ctx: IfBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.doBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitDoBlock?: (ctx: DoBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.navBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitNavBlock?: (ctx: NavBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.conditionBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitConditionBlock?: (ctx: ConditionBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.actionBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitActionBlock?: (ctx: ActionBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.waypointBlock`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitWaypointBlock?: (ctx: WaypointBlockContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.argsList`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitArgsList?: (ctx: ArgsListContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.arg`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitArg?: (ctx: ArgContext) => Result;
    /**
     * Visit a parse tree produced by `MetaFParser.block_id`.
     * @param ctx the parse tree
     * @return the visitor result
     */
    visitBlock_id?: (ctx: Block_idContext) => Result;
}

