// Generated from ./MetaFParser.g4 by ANTLR 4.13.1

import * as antlr from "antlr4ng";
import { Token } from "antlr4ng";

import { MetaFParserListener } from "./MetaFParserListener.js";
import { MetaFParserVisitor } from "./MetaFParserVisitor.js";

// for running tests with parameters, TODO: discuss strategy for typed parameters in CI
// eslint-disable-next-line no-unused-vars
type int = number;


export class MetaFParser extends antlr.Parser {
    public static readonly INDENT = 1;
    public static readonly DEDENT = 2;
    public static readonly CURLYCONTENTS = 3;
    public static readonly NOT_KEYWORD = 4;
    public static readonly STATE_LABEL = 5;
    public static readonly NAV_LABEL = 6;
    public static readonly IF_LABEL = 7;
    public static readonly DO_LABEL = 8;
    public static readonly COLON = 9;
    public static readonly OPENCURL = 10;
    public static readonly CLOSECURL = 11;
    public static readonly PLUS = 12;
    public static readonly MINUS = 13;
    public static readonly HEX = 14;
    public static readonly NUMBER = 15;
    public static readonly DECIMAL = 16;
    public static readonly IDENTIFIER = 17;
    public static readonly COMMENT = 18;
    public static readonly NL = 19;
    public static readonly WS = 20;
    public static readonly ANY = 21;
    public static readonly RULE_meta = 0;
    public static readonly RULE_stateBlock = 1;
    public static readonly RULE_ifBlock = 2;
    public static readonly RULE_doBlock = 3;
    public static readonly RULE_navBlock = 4;
    public static readonly RULE_conditionBlock = 5;
    public static readonly RULE_actionBlock = 6;
    public static readonly RULE_waypointBlock = 7;
    public static readonly RULE_argsList = 8;
    public static readonly RULE_arg = 9;
    public static readonly RULE_block_id = 10;

    public static readonly literalNames = [
        null, null, null, null, "'Not'", "'STATE'", "'NAV'", "'IF'", "'DO'", 
        "':'", "'{'", "'}'", "'+'", "'-'"
    ];

    public static readonly symbolicNames = [
        null, "INDENT", "DEDENT", "CURLYCONTENTS", "NOT_KEYWORD", "STATE_LABEL", 
        "NAV_LABEL", "IF_LABEL", "DO_LABEL", "COLON", "OPENCURL", "CLOSECURL", 
        "PLUS", "MINUS", "HEX", "NUMBER", "DECIMAL", "IDENTIFIER", "COMMENT", 
        "NL", "WS", "ANY"
    ];
    public static readonly ruleNames = [
        "meta", "stateBlock", "ifBlock", "doBlock", "navBlock", "conditionBlock", 
        "actionBlock", "waypointBlock", "argsList", "arg", "block_id",
    ];

    public get grammarFileName(): string { return "MetaFParser.g4"; }
    public get literalNames(): (string | null)[] { return MetaFParser.literalNames; }
    public get symbolicNames(): (string | null)[] { return MetaFParser.symbolicNames; }
    public get ruleNames(): string[] { return MetaFParser.ruleNames; }
    public get serializedATN(): number[] { return MetaFParser._serializedATN; }

    protected createFailedPredicateException(predicate?: string, message?: string): antlr.FailedPredicateException {
        return new antlr.FailedPredicateException(this, predicate, message);
    }

    public constructor(input: antlr.TokenStream) {
        super(input);
        this.interpreter = new antlr.ParserATNSimulator(this, MetaFParser._ATN, MetaFParser.decisionsToDFA, new antlr.PredictionContextCache());
    }
    public meta(): MetaContext {
        let localContext = new MetaContext(this.context, this.state);
        this.enterRule(localContext, 0, MetaFParser.RULE_meta);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 27;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 524384) !== 0)) {
                {
                this.state = 25;
                this.errorHandler.sync(this);
                switch (this.tokenStream.LA(1)) {
                case MetaFParser.STATE_LABEL:
                    {
                    this.state = 22;
                    this.stateBlock();
                    }
                    break;
                case MetaFParser.NAV_LABEL:
                    {
                    this.state = 23;
                    this.navBlock();
                    }
                    break;
                case MetaFParser.NL:
                    {
                    this.state = 24;
                    this.match(MetaFParser.NL);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
                }
                }
                this.state = 29;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 30;
            this.match(MetaFParser.EOF);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public stateBlock(): StateBlockContext {
        let localContext = new StateBlockContext(this.context, this.state);
        this.enterRule(localContext, 2, MetaFParser.RULE_stateBlock);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 32;
            this.match(MetaFParser.STATE_LABEL);
            this.state = 33;
            this.match(MetaFParser.COLON);
            this.state = 34;
            this.argsList();
            this.state = 38;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 20) {
                {
                {
                this.state = 35;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 40;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 41;
            this.match(MetaFParser.INDENT);
            this.state = 46;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 7 || _la === 19) {
                {
                this.state = 44;
                this.errorHandler.sync(this);
                switch (this.tokenStream.LA(1)) {
                case MetaFParser.IF_LABEL:
                    {
                    this.state = 42;
                    this.ifBlock();
                    }
                    break;
                case MetaFParser.NL:
                    {
                    this.state = 43;
                    this.match(MetaFParser.NL);
                    }
                    break;
                default:
                    throw new antlr.NoViableAltException(this);
                }
                }
                this.state = 48;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 49;
            this.match(MetaFParser.DEDENT);
            this.state = 51;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 5, this.context) ) {
            case 1:
                {
                this.state = 50;
                this.match(MetaFParser.NL);
                }
                break;
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public ifBlock(): IfBlockContext {
        let localContext = new IfBlockContext(this.context, this.state);
        this.enterRule(localContext, 4, MetaFParser.RULE_ifBlock);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 53;
            this.match(MetaFParser.IF_LABEL);
            this.state = 54;
            this.match(MetaFParser.COLON);
            this.state = 56;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            do {
                {
                {
                this.state = 55;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 58;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            } while (_la === 20);
            this.state = 60;
            this.conditionBlock();
            this.state = 64;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 20) {
                {
                {
                this.state = 61;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 66;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 67;
            this.match(MetaFParser.INDENT);
            this.state = 71;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 19) {
                {
                {
                this.state = 68;
                this.match(MetaFParser.NL);
                }
                }
                this.state = 73;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 74;
            this.doBlock();
            this.state = 75;
            this.match(MetaFParser.DEDENT);
            this.state = 77;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 9, this.context) ) {
            case 1:
                {
                this.state = 76;
                this.match(MetaFParser.NL);
                }
                break;
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public doBlock(): DoBlockContext {
        let localContext = new DoBlockContext(this.context, this.state);
        this.enterRule(localContext, 6, MetaFParser.RULE_doBlock);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 79;
            this.match(MetaFParser.DO_LABEL);
            this.state = 80;
            this.match(MetaFParser.COLON);
            this.state = 82;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            do {
                {
                {
                this.state = 81;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 84;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            } while (_la === 20);
            this.state = 86;
            this.actionBlock();
            this.state = 90;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 20) {
                {
                {
                this.state = 87;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 92;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 94;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 19) {
                {
                this.state = 93;
                this.match(MetaFParser.NL);
                }
            }

            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public navBlock(): NavBlockContext {
        let localContext = new NavBlockContext(this.context, this.state);
        this.enterRule(localContext, 8, MetaFParser.RULE_navBlock);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 96;
            this.match(MetaFParser.NAV_LABEL);
            this.state = 97;
            this.match(MetaFParser.COLON);
            this.state = 98;
            this.argsList();
            this.state = 102;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 20) {
                {
                {
                this.state = 99;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 104;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 114;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 1) {
                {
                this.state = 105;
                this.match(MetaFParser.INDENT);
                this.state = 110;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                while (_la === 17 || _la === 19) {
                    {
                    this.state = 108;
                    this.errorHandler.sync(this);
                    switch (this.tokenStream.LA(1)) {
                    case MetaFParser.IDENTIFIER:
                        {
                        this.state = 106;
                        this.waypointBlock();
                        }
                        break;
                    case MetaFParser.NL:
                        {
                        this.state = 107;
                        this.match(MetaFParser.NL);
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                    }
                    }
                    this.state = 112;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                }
                this.state = 113;
                this.match(MetaFParser.DEDENT);
                }
            }

            this.state = 117;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 17, this.context) ) {
            case 1:
                {
                this.state = 116;
                this.match(MetaFParser.NL);
                }
                break;
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public conditionBlock(): ConditionBlockContext {
        let localContext = new ConditionBlockContext(this.context, this.state);
        this.enterRule(localContext, 10, MetaFParser.RULE_conditionBlock);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 127;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 4) {
                {
                {
                this.state = 119;
                this.match(MetaFParser.NOT_KEYWORD);
                this.state = 121;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                do {
                    {
                    {
                    this.state = 120;
                    this.match(MetaFParser.WS);
                    }
                    }
                    this.state = 123;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                } while (_la === 20);
                }
                }
                this.state = 129;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 130;
            this.block_id();
            this.state = 131;
            this.argsList();
            this.state = 135;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 20, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 132;
                    this.match(MetaFParser.WS);
                    }
                    }
                }
                this.state = 137;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 20, this.context);
            }
            this.state = 146;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 23, this.context) ) {
            case 1:
                {
                this.state = 138;
                this.match(MetaFParser.INDENT);
                this.state = 141;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                do {
                    {
                    this.state = 141;
                    this.errorHandler.sync(this);
                    switch (this.tokenStream.LA(1)) {
                    case MetaFParser.NOT_KEYWORD:
                    case MetaFParser.IDENTIFIER:
                        {
                        this.state = 139;
                        this.conditionBlock();
                        }
                        break;
                    case MetaFParser.NL:
                        {
                        this.state = 140;
                        this.match(MetaFParser.NL);
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                    }
                    }
                    this.state = 143;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                } while ((((_la) & ~0x1F) === 0 && ((1 << _la) & 655376) !== 0));
                this.state = 145;
                this.match(MetaFParser.DEDENT);
                }
                break;
            }
            this.state = 149;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 24, this.context) ) {
            case 1:
                {
                this.state = 148;
                this.match(MetaFParser.NL);
                }
                break;
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public actionBlock(): ActionBlockContext {
        let localContext = new ActionBlockContext(this.context, this.state);
        this.enterRule(localContext, 12, MetaFParser.RULE_actionBlock);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 151;
            this.block_id();
            this.state = 152;
            this.argsList();
            this.state = 156;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 25, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 153;
                    this.match(MetaFParser.WS);
                    }
                    }
                }
                this.state = 158;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 25, this.context);
            }
            this.state = 167;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            if (_la === 1) {
                {
                this.state = 159;
                this.match(MetaFParser.INDENT);
                this.state = 162;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
                do {
                    {
                    this.state = 162;
                    this.errorHandler.sync(this);
                    switch (this.tokenStream.LA(1)) {
                    case MetaFParser.IDENTIFIER:
                        {
                        this.state = 160;
                        this.actionBlock();
                        }
                        break;
                    case MetaFParser.NL:
                        {
                        this.state = 161;
                        this.match(MetaFParser.NL);
                        }
                        break;
                    default:
                        throw new antlr.NoViableAltException(this);
                    }
                    }
                    this.state = 164;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                } while (_la === 17 || _la === 19);
                this.state = 166;
                this.match(MetaFParser.DEDENT);
                }
            }

            this.state = 170;
            this.errorHandler.sync(this);
            switch (this.interpreter.adaptivePredict(this.tokenStream, 29, this.context) ) {
            case 1:
                {
                this.state = 169;
                this.match(MetaFParser.NL);
                }
                break;
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public waypointBlock(): WaypointBlockContext {
        let localContext = new WaypointBlockContext(this.context, this.state);
        this.enterRule(localContext, 14, MetaFParser.RULE_waypointBlock);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 172;
            this.block_id();
            this.state = 173;
            this.argsList();
            this.state = 177;
            this.errorHandler.sync(this);
            _la = this.tokenStream.LA(1);
            while (_la === 20) {
                {
                {
                this.state = 174;
                this.match(MetaFParser.WS);
                }
                }
                this.state = 179;
                this.errorHandler.sync(this);
                _la = this.tokenStream.LA(1);
            }
            this.state = 180;
            this.match(MetaFParser.NL);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public argsList(): ArgsListContext {
        let localContext = new ArgsListContext(this.context, this.state);
        this.enterRule(localContext, 16, MetaFParser.RULE_argsList);
        let _la: number;
        try {
            let alternative: number;
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 190;
            this.errorHandler.sync(this);
            alternative = this.interpreter.adaptivePredict(this.tokenStream, 32, this.context);
            while (alternative !== 2 && alternative !== antlr.ATN.INVALID_ALT_NUMBER) {
                if (alternative === 1) {
                    {
                    {
                    this.state = 183;
                    this.errorHandler.sync(this);
                    _la = this.tokenStream.LA(1);
                    do {
                        {
                        {
                        this.state = 182;
                        this.match(MetaFParser.WS);
                        }
                        }
                        this.state = 185;
                        this.errorHandler.sync(this);
                        _la = this.tokenStream.LA(1);
                    } while (_la === 20);
                    this.state = 187;
                    this.arg();
                    }
                    }
                }
                this.state = 192;
                this.errorHandler.sync(this);
                alternative = this.interpreter.adaptivePredict(this.tokenStream, 32, this.context);
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public arg(): ArgContext {
        let localContext = new ArgContext(this.context, this.state);
        this.enterRule(localContext, 18, MetaFParser.RULE_arg);
        let _la: number;
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 193;
            localContext._value = this.tokenStream.LT(1);
            _la = this.tokenStream.LA(1);
            if(!((((_la) & ~0x1F) === 0 && ((1 << _la) & 245768) !== 0))) {
                localContext._value = this.errorHandler.recoverInline(this);
            }
            else {
                this.errorHandler.reportMatch(this);
                this.consume();
            }
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }
    public block_id(): Block_idContext {
        let localContext = new Block_idContext(this.context, this.state);
        this.enterRule(localContext, 20, MetaFParser.RULE_block_id);
        try {
            this.enterOuterAlt(localContext, 1);
            {
            this.state = 195;
            this.match(MetaFParser.IDENTIFIER);
            }
        }
        catch (re) {
            if (re instanceof antlr.RecognitionException) {
                localContext.exception = re;
                this.errorHandler.reportError(this, re);
                this.errorHandler.recover(this, re);
            } else {
                throw re;
            }
        }
        finally {
            this.exitRule();
        }
        return localContext;
    }

    public static readonly _serializedATN: number[] = [
        4,1,21,198,2,0,7,0,2,1,7,1,2,2,7,2,2,3,7,3,2,4,7,4,2,5,7,5,2,6,7,
        6,2,7,7,7,2,8,7,8,2,9,7,9,2,10,7,10,1,0,1,0,1,0,5,0,26,8,0,10,0,
        12,0,29,9,0,1,0,1,0,1,1,1,1,1,1,1,1,5,1,37,8,1,10,1,12,1,40,9,1,
        1,1,1,1,1,1,5,1,45,8,1,10,1,12,1,48,9,1,1,1,1,1,3,1,52,8,1,1,2,1,
        2,1,2,4,2,57,8,2,11,2,12,2,58,1,2,1,2,5,2,63,8,2,10,2,12,2,66,9,
        2,1,2,1,2,5,2,70,8,2,10,2,12,2,73,9,2,1,2,1,2,1,2,3,2,78,8,2,1,3,
        1,3,1,3,4,3,83,8,3,11,3,12,3,84,1,3,1,3,5,3,89,8,3,10,3,12,3,92,
        9,3,1,3,3,3,95,8,3,1,4,1,4,1,4,1,4,5,4,101,8,4,10,4,12,4,104,9,4,
        1,4,1,4,1,4,5,4,109,8,4,10,4,12,4,112,9,4,1,4,3,4,115,8,4,1,4,3,
        4,118,8,4,1,5,1,5,4,5,122,8,5,11,5,12,5,123,5,5,126,8,5,10,5,12,
        5,129,9,5,1,5,1,5,1,5,5,5,134,8,5,10,5,12,5,137,9,5,1,5,1,5,1,5,
        4,5,142,8,5,11,5,12,5,143,1,5,3,5,147,8,5,1,5,3,5,150,8,5,1,6,1,
        6,1,6,5,6,155,8,6,10,6,12,6,158,9,6,1,6,1,6,1,6,4,6,163,8,6,11,6,
        12,6,164,1,6,3,6,168,8,6,1,6,3,6,171,8,6,1,7,1,7,1,7,5,7,176,8,7,
        10,7,12,7,179,9,7,1,7,1,7,1,8,4,8,184,8,8,11,8,12,8,185,1,8,5,8,
        189,8,8,10,8,12,8,192,9,8,1,9,1,9,1,10,1,10,1,10,0,0,11,0,2,4,6,
        8,10,12,14,16,18,20,0,1,2,0,3,3,14,17,220,0,27,1,0,0,0,2,32,1,0,
        0,0,4,53,1,0,0,0,6,79,1,0,0,0,8,96,1,0,0,0,10,127,1,0,0,0,12,151,
        1,0,0,0,14,172,1,0,0,0,16,190,1,0,0,0,18,193,1,0,0,0,20,195,1,0,
        0,0,22,26,3,2,1,0,23,26,3,8,4,0,24,26,5,19,0,0,25,22,1,0,0,0,25,
        23,1,0,0,0,25,24,1,0,0,0,26,29,1,0,0,0,27,25,1,0,0,0,27,28,1,0,0,
        0,28,30,1,0,0,0,29,27,1,0,0,0,30,31,5,0,0,1,31,1,1,0,0,0,32,33,5,
        5,0,0,33,34,5,9,0,0,34,38,3,16,8,0,35,37,5,20,0,0,36,35,1,0,0,0,
        37,40,1,0,0,0,38,36,1,0,0,0,38,39,1,0,0,0,39,41,1,0,0,0,40,38,1,
        0,0,0,41,46,5,1,0,0,42,45,3,4,2,0,43,45,5,19,0,0,44,42,1,0,0,0,44,
        43,1,0,0,0,45,48,1,0,0,0,46,44,1,0,0,0,46,47,1,0,0,0,47,49,1,0,0,
        0,48,46,1,0,0,0,49,51,5,2,0,0,50,52,5,19,0,0,51,50,1,0,0,0,51,52,
        1,0,0,0,52,3,1,0,0,0,53,54,5,7,0,0,54,56,5,9,0,0,55,57,5,20,0,0,
        56,55,1,0,0,0,57,58,1,0,0,0,58,56,1,0,0,0,58,59,1,0,0,0,59,60,1,
        0,0,0,60,64,3,10,5,0,61,63,5,20,0,0,62,61,1,0,0,0,63,66,1,0,0,0,
        64,62,1,0,0,0,64,65,1,0,0,0,65,67,1,0,0,0,66,64,1,0,0,0,67,71,5,
        1,0,0,68,70,5,19,0,0,69,68,1,0,0,0,70,73,1,0,0,0,71,69,1,0,0,0,71,
        72,1,0,0,0,72,74,1,0,0,0,73,71,1,0,0,0,74,75,3,6,3,0,75,77,5,2,0,
        0,76,78,5,19,0,0,77,76,1,0,0,0,77,78,1,0,0,0,78,5,1,0,0,0,79,80,
        5,8,0,0,80,82,5,9,0,0,81,83,5,20,0,0,82,81,1,0,0,0,83,84,1,0,0,0,
        84,82,1,0,0,0,84,85,1,0,0,0,85,86,1,0,0,0,86,90,3,12,6,0,87,89,5,
        20,0,0,88,87,1,0,0,0,89,92,1,0,0,0,90,88,1,0,0,0,90,91,1,0,0,0,91,
        94,1,0,0,0,92,90,1,0,0,0,93,95,5,19,0,0,94,93,1,0,0,0,94,95,1,0,
        0,0,95,7,1,0,0,0,96,97,5,6,0,0,97,98,5,9,0,0,98,102,3,16,8,0,99,
        101,5,20,0,0,100,99,1,0,0,0,101,104,1,0,0,0,102,100,1,0,0,0,102,
        103,1,0,0,0,103,114,1,0,0,0,104,102,1,0,0,0,105,110,5,1,0,0,106,
        109,3,14,7,0,107,109,5,19,0,0,108,106,1,0,0,0,108,107,1,0,0,0,109,
        112,1,0,0,0,110,108,1,0,0,0,110,111,1,0,0,0,111,113,1,0,0,0,112,
        110,1,0,0,0,113,115,5,2,0,0,114,105,1,0,0,0,114,115,1,0,0,0,115,
        117,1,0,0,0,116,118,5,19,0,0,117,116,1,0,0,0,117,118,1,0,0,0,118,
        9,1,0,0,0,119,121,5,4,0,0,120,122,5,20,0,0,121,120,1,0,0,0,122,123,
        1,0,0,0,123,121,1,0,0,0,123,124,1,0,0,0,124,126,1,0,0,0,125,119,
        1,0,0,0,126,129,1,0,0,0,127,125,1,0,0,0,127,128,1,0,0,0,128,130,
        1,0,0,0,129,127,1,0,0,0,130,131,3,20,10,0,131,135,3,16,8,0,132,134,
        5,20,0,0,133,132,1,0,0,0,134,137,1,0,0,0,135,133,1,0,0,0,135,136,
        1,0,0,0,136,146,1,0,0,0,137,135,1,0,0,0,138,141,5,1,0,0,139,142,
        3,10,5,0,140,142,5,19,0,0,141,139,1,0,0,0,141,140,1,0,0,0,142,143,
        1,0,0,0,143,141,1,0,0,0,143,144,1,0,0,0,144,145,1,0,0,0,145,147,
        5,2,0,0,146,138,1,0,0,0,146,147,1,0,0,0,147,149,1,0,0,0,148,150,
        5,19,0,0,149,148,1,0,0,0,149,150,1,0,0,0,150,11,1,0,0,0,151,152,
        3,20,10,0,152,156,3,16,8,0,153,155,5,20,0,0,154,153,1,0,0,0,155,
        158,1,0,0,0,156,154,1,0,0,0,156,157,1,0,0,0,157,167,1,0,0,0,158,
        156,1,0,0,0,159,162,5,1,0,0,160,163,3,12,6,0,161,163,5,19,0,0,162,
        160,1,0,0,0,162,161,1,0,0,0,163,164,1,0,0,0,164,162,1,0,0,0,164,
        165,1,0,0,0,165,166,1,0,0,0,166,168,5,2,0,0,167,159,1,0,0,0,167,
        168,1,0,0,0,168,170,1,0,0,0,169,171,5,19,0,0,170,169,1,0,0,0,170,
        171,1,0,0,0,171,13,1,0,0,0,172,173,3,20,10,0,173,177,3,16,8,0,174,
        176,5,20,0,0,175,174,1,0,0,0,176,179,1,0,0,0,177,175,1,0,0,0,177,
        178,1,0,0,0,178,180,1,0,0,0,179,177,1,0,0,0,180,181,5,19,0,0,181,
        15,1,0,0,0,182,184,5,20,0,0,183,182,1,0,0,0,184,185,1,0,0,0,185,
        183,1,0,0,0,185,186,1,0,0,0,186,187,1,0,0,0,187,189,3,18,9,0,188,
        183,1,0,0,0,189,192,1,0,0,0,190,188,1,0,0,0,190,191,1,0,0,0,191,
        17,1,0,0,0,192,190,1,0,0,0,193,194,7,0,0,0,194,19,1,0,0,0,195,196,
        5,17,0,0,196,21,1,0,0,0,33,25,27,38,44,46,51,58,64,71,77,84,90,94,
        102,108,110,114,117,123,127,135,141,143,146,149,156,162,164,167,
        170,177,185,190
    ];

    private static __ATN: antlr.ATN;
    public static get _ATN(): antlr.ATN {
        if (!MetaFParser.__ATN) {
            MetaFParser.__ATN = new antlr.ATNDeserializer().deserialize(MetaFParser._serializedATN);
        }

        return MetaFParser.__ATN;
    }


    private static readonly vocabulary = new antlr.Vocabulary(MetaFParser.literalNames, MetaFParser.symbolicNames, []);

    public override get vocabulary(): antlr.Vocabulary {
        return MetaFParser.vocabulary;
    }

    private static readonly decisionsToDFA = MetaFParser._ATN.decisionToState.map( (ds: antlr.DecisionState, index: number) => new antlr.DFA(ds, index) );
}

export class MetaContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public EOF(): antlr.TerminalNode {
        return this.getToken(MetaFParser.EOF, 0)!;
    }
    public stateBlock(): StateBlockContext[];
    public stateBlock(i: number): StateBlockContext | null;
    public stateBlock(i?: number): StateBlockContext[] | StateBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(StateBlockContext);
        }

        return this.getRuleContext(i, StateBlockContext);
    }
    public navBlock(): NavBlockContext[];
    public navBlock(i: number): NavBlockContext | null;
    public navBlock(i?: number): NavBlockContext[] | NavBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(NavBlockContext);
        }

        return this.getRuleContext(i, NavBlockContext);
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_meta;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterMeta) {
             listener.enterMeta(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitMeta) {
             listener.exitMeta(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitMeta) {
            return visitor.visitMeta(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class StateBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public STATE_LABEL(): antlr.TerminalNode {
        return this.getToken(MetaFParser.STATE_LABEL, 0)!;
    }
    public COLON(): antlr.TerminalNode {
        return this.getToken(MetaFParser.COLON, 0)!;
    }
    public argsList(): ArgsListContext {
        return this.getRuleContext(0, ArgsListContext)!;
    }
    public INDENT(): antlr.TerminalNode {
        return this.getToken(MetaFParser.INDENT, 0)!;
    }
    public DEDENT(): antlr.TerminalNode {
        return this.getToken(MetaFParser.DEDENT, 0)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public ifBlock(): IfBlockContext[];
    public ifBlock(i: number): IfBlockContext | null;
    public ifBlock(i?: number): IfBlockContext[] | IfBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(IfBlockContext);
        }

        return this.getRuleContext(i, IfBlockContext);
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_stateBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterStateBlock) {
             listener.enterStateBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitStateBlock) {
             listener.exitStateBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitStateBlock) {
            return visitor.visitStateBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class IfBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public IF_LABEL(): antlr.TerminalNode {
        return this.getToken(MetaFParser.IF_LABEL, 0)!;
    }
    public COLON(): antlr.TerminalNode {
        return this.getToken(MetaFParser.COLON, 0)!;
    }
    public conditionBlock(): ConditionBlockContext {
        return this.getRuleContext(0, ConditionBlockContext)!;
    }
    public INDENT(): antlr.TerminalNode {
        return this.getToken(MetaFParser.INDENT, 0)!;
    }
    public doBlock(): DoBlockContext {
        return this.getRuleContext(0, DoBlockContext)!;
    }
    public DEDENT(): antlr.TerminalNode {
        return this.getToken(MetaFParser.DEDENT, 0)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_ifBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterIfBlock) {
             listener.enterIfBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitIfBlock) {
             listener.exitIfBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitIfBlock) {
            return visitor.visitIfBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class DoBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public DO_LABEL(): antlr.TerminalNode {
        return this.getToken(MetaFParser.DO_LABEL, 0)!;
    }
    public COLON(): antlr.TerminalNode {
        return this.getToken(MetaFParser.COLON, 0)!;
    }
    public actionBlock(): ActionBlockContext {
        return this.getRuleContext(0, ActionBlockContext)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public NL(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.NL, 0);
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_doBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterDoBlock) {
             listener.enterDoBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitDoBlock) {
             listener.exitDoBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitDoBlock) {
            return visitor.visitDoBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class NavBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public NAV_LABEL(): antlr.TerminalNode {
        return this.getToken(MetaFParser.NAV_LABEL, 0)!;
    }
    public COLON(): antlr.TerminalNode {
        return this.getToken(MetaFParser.COLON, 0)!;
    }
    public argsList(): ArgsListContext {
        return this.getRuleContext(0, ArgsListContext)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public INDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.INDENT, 0);
    }
    public DEDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.DEDENT, 0);
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public waypointBlock(): WaypointBlockContext[];
    public waypointBlock(i: number): WaypointBlockContext | null;
    public waypointBlock(i?: number): WaypointBlockContext[] | WaypointBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(WaypointBlockContext);
        }

        return this.getRuleContext(i, WaypointBlockContext);
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_navBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterNavBlock) {
             listener.enterNavBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitNavBlock) {
             listener.exitNavBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitNavBlock) {
            return visitor.visitNavBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ConditionBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public block_id(): Block_idContext {
        return this.getRuleContext(0, Block_idContext)!;
    }
    public argsList(): ArgsListContext {
        return this.getRuleContext(0, ArgsListContext)!;
    }
    public NOT_KEYWORD(): antlr.TerminalNode[];
    public NOT_KEYWORD(i: number): antlr.TerminalNode | null;
    public NOT_KEYWORD(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NOT_KEYWORD);
    	} else {
    		return this.getToken(MetaFParser.NOT_KEYWORD, i);
    	}
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public INDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.INDENT, 0);
    }
    public DEDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.DEDENT, 0);
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public conditionBlock(): ConditionBlockContext[];
    public conditionBlock(i: number): ConditionBlockContext | null;
    public conditionBlock(i?: number): ConditionBlockContext[] | ConditionBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ConditionBlockContext);
        }

        return this.getRuleContext(i, ConditionBlockContext);
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_conditionBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterConditionBlock) {
             listener.enterConditionBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitConditionBlock) {
             listener.exitConditionBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitConditionBlock) {
            return visitor.visitConditionBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ActionBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public block_id(): Block_idContext {
        return this.getRuleContext(0, Block_idContext)!;
    }
    public argsList(): ArgsListContext {
        return this.getRuleContext(0, ArgsListContext)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public INDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.INDENT, 0);
    }
    public DEDENT(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.DEDENT, 0);
    }
    public NL(): antlr.TerminalNode[];
    public NL(i: number): antlr.TerminalNode | null;
    public NL(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.NL);
    	} else {
    		return this.getToken(MetaFParser.NL, i);
    	}
    }
    public actionBlock(): ActionBlockContext[];
    public actionBlock(i: number): ActionBlockContext | null;
    public actionBlock(i?: number): ActionBlockContext[] | ActionBlockContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ActionBlockContext);
        }

        return this.getRuleContext(i, ActionBlockContext);
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_actionBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterActionBlock) {
             listener.enterActionBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitActionBlock) {
             listener.exitActionBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitActionBlock) {
            return visitor.visitActionBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class WaypointBlockContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public block_id(): Block_idContext {
        return this.getRuleContext(0, Block_idContext)!;
    }
    public argsList(): ArgsListContext {
        return this.getRuleContext(0, ArgsListContext)!;
    }
    public NL(): antlr.TerminalNode {
        return this.getToken(MetaFParser.NL, 0)!;
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_waypointBlock;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterWaypointBlock) {
             listener.enterWaypointBlock(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitWaypointBlock) {
             listener.exitWaypointBlock(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitWaypointBlock) {
            return visitor.visitWaypointBlock(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ArgsListContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public arg(): ArgContext[];
    public arg(i: number): ArgContext | null;
    public arg(i?: number): ArgContext[] | ArgContext | null {
        if (i === undefined) {
            return this.getRuleContexts(ArgContext);
        }

        return this.getRuleContext(i, ArgContext);
    }
    public WS(): antlr.TerminalNode[];
    public WS(i: number): antlr.TerminalNode | null;
    public WS(i?: number): antlr.TerminalNode | null | antlr.TerminalNode[] {
    	if (i === undefined) {
    		return this.getTokens(MetaFParser.WS);
    	} else {
    		return this.getToken(MetaFParser.WS, i);
    	}
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_argsList;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterArgsList) {
             listener.enterArgsList(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitArgsList) {
             listener.exitArgsList(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitArgsList) {
            return visitor.visitArgsList(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class ArgContext extends antlr.ParserRuleContext {
    public _value: Token | null;
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public NUMBER(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.NUMBER, 0);
    }
    public DECIMAL(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.DECIMAL, 0);
    }
    public HEX(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.HEX, 0);
    }
    public CURLYCONTENTS(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.CURLYCONTENTS, 0);
    }
    public IDENTIFIER(): antlr.TerminalNode | null {
        return this.getToken(MetaFParser.IDENTIFIER, 0);
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_arg;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterArg) {
             listener.enterArg(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitArg) {
             listener.exitArg(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitArg) {
            return visitor.visitArg(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}


export class Block_idContext extends antlr.ParserRuleContext {
    public constructor(parent: antlr.ParserRuleContext | null, invokingState: number) {
        super(parent, invokingState);
    }
    public IDENTIFIER(): antlr.TerminalNode {
        return this.getToken(MetaFParser.IDENTIFIER, 0)!;
    }
    public override get ruleIndex(): number {
        return MetaFParser.RULE_block_id;
    }
    public override enterRule(listener: MetaFParserListener): void {
        if(listener.enterBlock_id) {
             listener.enterBlock_id(this);
        }
    }
    public override exitRule(listener: MetaFParserListener): void {
        if(listener.exitBlock_id) {
             listener.exitBlock_id(this);
        }
    }
    public override accept<Result>(visitor: MetaFParserVisitor<Result>): Result | null {
        if (visitor.visitBlock_id) {
            return visitor.visitBlock_id(this);
        } else {
            return visitor.visitChildren(this);
        }
    }
}
