import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import WaypointBlock from "./WaypointBlock";

export default abstract class PositionWaypointBlock extends WaypointBlock {
  PositionArguments: BlockArgument[] = [
    {
      Name: "X",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world x coordinate of this nav node"
    },
    {
      Name: "Y",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world y coordinate of this nav node"
    },
    {
      Name: "Z",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world z coordinate of this nav node"
    }
  ] as BlockArgument[];
  
  public X: number = 0;
  public Y: number = 0;
  public Z: number = 0;
}