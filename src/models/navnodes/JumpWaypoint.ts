import { Token } from "antlr4ng";
import { ArgContext } from "../../gen/MetaFParser";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import PositionWaypointBlock from "./PositionWaypointBlock";
import ParserHelpers from "../../lib/TokenValueResolvers";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafVarType from "../MetafVarType";

export default class JumpWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'jmp';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "Heading",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The heading to face before performing the jump."
    },
    {
      Name: "Walk",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "Set to True to for a walk-jump, False for a run-jump.",
      ValueResolver: ParserHelpers.resolveBoolean
    },
    {
      Name: "Power",
      ValidTokenTypes: [ MetaFLexer.NUMBER, MetaFLexer.DECIMAL ],
      Type: MetafVarType.Number,
      Description: "The jump power / time. This is a value in milliseconds from 0-1000.",
      ValueResolver: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
        const value = visitor.visitArg(ctx, token) as number;
        if (value < 0 || value > 1000) {
          visitor.reportError(ctx, MetafErrorType.InvalidArgumentType, `Jump Power should be between 0-1000.`);
        }
        return value;
      }
    }
  ] as BlockArgument[];

  public Heading: number;
  public Walk: boolean;
  public Power: number;
}