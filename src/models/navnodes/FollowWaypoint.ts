import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import WaypointBlock from "./WaypointBlock";
import ParserHelpers from "../../lib/TokenValueResolvers";
import MetafVarType from "../MetafVarType";

export default class FollowWaypoint extends WaypointBlock {
  IdentifierName: string = 'flw';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "TargetId",
      ValidTokenTypes: [ MetaFLexer.HEX, MetaFLexer.NUMBER ],
      Type: MetafVarType.Hex,
      Description: "The id of the character to follow, in hexadecimal.",
      ValueResolver: ParserHelpers.resolveObjectId
    },
    {
      Name: "TargetName",
      Type: MetafVarType.CurlyString,
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Description: "The name of the target object to follow."
    }
  ] as BlockArgument[];

  public TargetId: number;
  public TargetName: string;
}