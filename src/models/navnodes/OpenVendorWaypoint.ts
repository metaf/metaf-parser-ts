import { MetaFLexer } from "../../gen/MetaFlexer";
import ParserHelpers from "../../lib/TokenValueResolvers";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class OpenVendorWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'vnd';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "VendorId",
      ValidTokenTypes: [ MetaFLexer.HEX, MetaFLexer.NUMBER ],
      Type: MetafVarType.Hex,
      Description: "The ObjectId of the vendor to open.",
      ValueResolver: ParserHelpers.resolveObjectId
    },
    {
      Name: "VendorName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The name of the vendor."
    }
  ] as BlockArgument[];

  public VendorId: number;
  public VendorName: string;
}