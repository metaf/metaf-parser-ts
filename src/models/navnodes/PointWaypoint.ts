import BlockArgument from "../BlockArgument";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class PointWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'pnt';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments
  ] as BlockArgument[];
}