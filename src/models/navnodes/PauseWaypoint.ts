import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class PauseWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'pau';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "Milliseconds",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      Description: "The number of milliseconds to pause for. There are 1000 milliseconds in a second."
    }
  ] as BlockArgument[];

  public Milliseconds: number;
}