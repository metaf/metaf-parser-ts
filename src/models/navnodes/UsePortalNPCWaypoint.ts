import { MetaFLexer } from "../../gen/MetaFlexer";
import ParserHelpers from "../../lib/TokenValueResolvers";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class UsePortalNPCWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'ptl';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "TargetX",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world x coordinate of the target portal/npc"
    },
    {
      Name: "TargetY",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world y coordinate of the target portal/npc"
    },
    {
      Name: "TargetZ",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world z coordinate of the target portal/npc"
    },
    {
      Name: "ObjectClass",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      CompletionType: MetafCompletionType.ObjectClass,
      Description: "The ObjectClass of the target portal/npc.",
      ValueResolver: ParserHelpers.resolveObjectClass,
    },
    {
      Name: "Name",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The name of the target portal/npc to use."
    },
  ] as BlockArgument[];

  public ObjectClass: number;
  public Name: string;
  public TargetX: number;
  public TargetY: number;
  public TargetZ: number;
}