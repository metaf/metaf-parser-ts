import { Token } from "antlr4ng";
import { ArgContext } from "../../gen/MetaFParser";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import PositionWaypointBlock from "./PositionWaypointBlock";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class RecallSpellWaypoint extends PositionWaypointBlock {
  public static ValidRecallSpellNames: string[] = [
    'Aerlinthe Recall', 'Bur Recall', 'Call of the Mhoire Forge', 'Celestial Hand Stronghold Recall', 'Colosseum Recall',
    'Eldrytch Web Stronghold Recall', 'Facility Hub Recall', 'Gear Knight Invasion Area Camp Recall', 'Glenden Wood Recall',
    'Lifestone Recall', 'Lost City of Neftet Recall', 'Mount Lethe Recall', 'Paradox-touched Olthoi Infested Area Recall',
    'Portal Recall', 'Primary Portal Recall', 'Radiant Blood Stronghold Recall', 'Recall Aphus Lassel', 'Recall the Sanctuary',
    'Recall to the Singularity Caul', 'Return to the Keep', 'Rynthid Recall', 'Secondary Portal Recall', 'Ulgrim\'s Recall',
    'Viridian Rise Great Tree Recall', 'Viridian Rise Recall'
  ];

  IdentifierName: string = 'rcl';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "SpellName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Description: "The full name of the recall spell to cast.",
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.RecallSpellName,
      ValueResolver: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
        const spellName = token.text;

        if (!RecallSpellWaypoint.ValidRecallSpellNames.includes(spellName)) {
          visitor.reportError(ctx, MetafErrorType.InvalidArgumentType, `Non-EOR Recall Spell: '${spellName}'. If you are playing on a custom content server it's possible this is valid, but this was not a spell at end of retail.`);
        }

        return spellName;
      },
    }
  ] as BlockArgument[];

  public SpellName: string;
}