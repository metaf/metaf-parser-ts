import BlockArgument from "../BlockArgument";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class CheckpointWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'chk';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments
  ] as BlockArgument[];
}