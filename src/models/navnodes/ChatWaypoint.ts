import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class ChatWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'cht';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "Text",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The message / command to send to the chat window."
    }
  ] as BlockArgument[];
  
  public Text: string;
}