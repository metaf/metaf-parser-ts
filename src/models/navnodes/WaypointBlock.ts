import BlockArgument from "../BlockArgument";
import { BlockBase } from "../BlockBase";
import { ParserModelBase } from "../ParserModelBase";

export default abstract class WaypointBlock extends BlockBase {
  readonly abstract IdentifierName: string;
  readonly abstract Arguments: BlockArgument[];
  readonly ValidChildren: (typeof ParserModelBase)[] = [];
}