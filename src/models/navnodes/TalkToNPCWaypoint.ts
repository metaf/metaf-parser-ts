import { Token } from "antlr4ng";
import { ArgContext } from "../../gen/MetaFParser";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import PositionWaypointBlock from "./PositionWaypointBlock";
import ParserHelpers from "../../lib/TokenValueResolvers";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class TalkToNPCWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'tlk';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    
    {
      Name: "TargetX",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world x coordinate of the target npc"
    },
    {
      Name: "TargetY",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world y coordinate of the target npc"
    },
    {
      Name: "TargetZ",
      ValidTokenTypes: [ MetaFLexer.DECIMAL, MetaFLexer.NUMBER ],
      Type: MetafVarType.Decimal,
      Description: "The global/world z coordinate of the target npc"
    },
    {
      Name: "ObjectClass",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      CompletionType: MetafCompletionType.ObjectClass,
      Description: "The ObjectClass of the target NPC.",
      ValueResolver: ParserHelpers.resolveObjectClass,
    },
    {
      Name: "Name",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The name of the NPC to talk to."
    }
  ] as BlockArgument[];

  public ObjectClass: number;
  public Name: string;
  public TargetX: number;
  public TargetY: number;
  public TargetZ: number;
}