import { MetaFLexer } from "../../gen/MetaFlexer";
import ParserHelpers from "../../lib/TokenValueResolvers";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import PositionWaypointBlock from "./PositionWaypointBlock";

export default class PortalWaypoint extends PositionWaypointBlock {
  IdentifierName: string = 'prt';
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [
    ...this.PositionArguments,
    {
      Name: "PortalId",
      ValidTokenTypes: [ MetaFLexer.HEX, MetaFLexer.NUMBER ],
      Type: MetafVarType.Hex,
      Description: "The ObjectId of the portal to use",
      ValueResolver: ParserHelpers.resolveObjectId
    }
  ] as BlockArgument[];

  public PortalId: number;

  public Deprecated: Boolean = true;
  public DeprecatedMessage: string = `prt navnode is deprecated! Use ptl (Use Portal/NPC) navnode instead!`;
}