
import { ConditionBlockContext } from "../../gen/MetaFParser";
import BlockArgument from "../BlockArgument";
import { BlockBase } from "../BlockBase";
import { ParserModelBase } from "../ParserModelBase";

export default abstract class ConditionBlock extends BlockBase {
  abstract IdentifierName: string;
  abstract Arguments: BlockArgument[];
  readonly ValidChildren: (typeof ParserModelBase)[] = [];
  public IsNotPrefixed: boolean = false;

  constructor(ctx: ConditionBlockContext, parent: ParserModelBase | undefined) {
    super(ctx, parent);
  }
}