
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class NeedToBuffCondition extends ConditionBlock {
  IdentifierName: string = 'NeedToBuff';
  Summary: string = "Check character buffs";
  Description: string = "Evaluates to true if VTank says your character needs any buffs.";
  Examples: string[] = [
   "~~True when VTank says you need buffs\nNeedToBuff"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}