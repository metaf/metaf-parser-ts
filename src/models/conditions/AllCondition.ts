import MultiConditionBase from "./MultiConditionBase";
import BlockArgument from "../BlockArgument";

export default class AllCondition extends MultiConditionBase {
  IdentifierName: string = 'All';
  Summary: string = "true if all child conditions are true.";
  Description: string = "Evaluates to true if all child conditions also evaluate to true.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [];
}