import { ParserRuleContext } from "antlr4ng";
import ConditionBlock from "./ConditionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";

export default class BuPercentGECondition extends ConditionBlock {
  IdentifierName: string = 'BuPercentGE';
  Summary: string = "Check character burden";
  Description: string = "Evaluates to true if your characters current burden percentage is greater than or equal to the passed percentage.";
  Examples: string[] = [
    "~~True if character burden is >= 110%.\nBuPercentGE 110"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Burden",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The burden percentage to compare your character to. Percentages are written out as whole numbers, where 123% is 123."
    }
  ] as BlockArgument[];
  
  public Burden: number | undefined;
}