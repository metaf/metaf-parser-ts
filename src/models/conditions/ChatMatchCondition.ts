
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class ChatMatchCondition extends ConditionBlock {
  IdentifierName: string = 'ChatMatch';
  Summary: string = "Chat text was matched";
  Description: string = "Evaluates to true if an incoming chat window message matched the specified pattern";
  Examples: string[] = [
    "Chat {^.*(?<who>Eskarina).* (says|tells you), \".+\"$} {2;4}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Pattern",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The regular expression to check against incoming chat messages."
    }
  ] as BlockArgument[];

  public Pattern: string;
}