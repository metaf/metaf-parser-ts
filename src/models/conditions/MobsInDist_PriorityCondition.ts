
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class MobsInDist_PriorityCondition extends ConditionBlock {
  IdentifierName: string = 'MobsInDist_Priority';
  Summary: string = "Check for priority mobs within distance";
  Description: string = "Evaluates to true if there are MobCount or greater mobs with the priority MobPriority within MobDistance yards of your character.";
  Examples: string[] = [
   "~~True when >=6 monsters of priority >=2 are within 4.7 yards of character..\nMobsInDist_Priority 6 4.7 2"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "MobCount",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The mob count to trigger against."
    },
    {
      Name: "MobDistance",
      ValidTokenTypes: [MetaFLexer.NUMBER, MetaFLexer.DECIMAL],
      Type: MetafVarType.Decimal,
      Description: "The distance to check against each mob."
    },
    {
      Name: "MobPriority",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "A priority (from the vtank monsters tab) to match against each mob. This is an exact match.",
    }
  ] as BlockArgument[];

  public MobPriority: number;
  public MobDistance: number;
  public MobCount: number;
}