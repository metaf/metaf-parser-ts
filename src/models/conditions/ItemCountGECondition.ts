
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class ItemCountGECondition extends ConditionBlock {
  IdentifierName: string = 'ItemCountGE';
  Summary: string = "Check inventory item count";
  Description: string = "Evaluates to true if you have greater than or equal ItemCount of the specified ItemName";
  Examples: string[] = [
   "~~True when your character has 25 or more Prismatic Tapers\nItemCountGE 25 {Prismatic Taper}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "ItemCount",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      Description: "The item count to trigger against.",
      IsOptional: false
    },
    {
      Name: "ItemName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.ItemName,
      Description: "The exact item name to check.",
    },
  ] as BlockArgument[];

  public ItemName: string;
  public ItemCount: number;
}