
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class PSecsInStateGECondition extends ConditionBlock {
  IdentifierName: string = 'PSecsInStateGE';
  Summary: string = "Check persistent time in state";
  Description: string = "Evaluates to true if you have been in this state more than Seconds seconds. Stopping the meta does not reset this counter.";
  Examples: string[] = [
    "~~True 15 seconds after entering (and staying in) the rule's state, whether or not the meta's execution is turned off/on.\nPSecsInStateGE 15"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Seconds",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The amount of seconds in this state until this condition triggers. Switching states and pausing the meta do not restart this counter."
    }
  ] as BlockArgument[];

  public Seconds: number;
}