
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class MainSlotsLECondition extends ConditionBlock {
  IdentifierName: string = 'MainSlotsLE';
  Summary: string = "Check inventory item count";
  Description: string = "Evaluates to true if you have less than or equal to SlotCount free main backpack slots";
  Examples: string[] = [
   "~~TTrue when <=7 inventory slots remain empty in character's main pack\nMainSlotsLE 7"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "SlotCount",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      Description: "The free main pack slot count to trigger against."
    }
  ] as BlockArgument[];

  public SlotCount: number;
}