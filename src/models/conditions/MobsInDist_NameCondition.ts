
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class MobsInDist_NameCondition extends ConditionBlock {
  IdentifierName: string = 'MobsInDist_Name';
  Summary: string = "Check for named mobs within distance";
  Description: string = "Evaluates to true if there are MobCount or greater mobs matching the pattern NamePattern within MobDistance yards of your character.";
  Examples: string[] = [
   "~~True when >=5 Drudge Lurkers are within 13.7 yards of character.\nMobsInDist_Name 5 13.7 {Drudge Lurker}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "MobCount",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The mob count to trigger against."
    },
    {
      Name: "MobDistance",
      ValidTokenTypes: [MetaFLexer.NUMBER, MetaFLexer.DECIMAL],
      Type: MetafVarType.Decimal,
      Description: "The distance to check against each mob."
    },
    {
      Name: "NamePattern",
      ValidTokenTypes: [MetaFLexer.CURLYCONTENTS],
      Type: MetafVarType.CurlyString,
      Description: "A regular expression to match against each mob's name."
    }
  ] as BlockArgument[];

  public NamePattern: string;
  public MobDistance: number;
  public MobCount: number;
}