
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class IntoPortalCondition extends ConditionBlock {
  IdentifierName: string = 'IntoPortal';
  Summary: string = "Character has entered portal space";
  Description: string = "Evaluates to true when your character enters portal space";
  Examples: string[] = [
   
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}