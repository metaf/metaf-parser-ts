
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class VendorClosedCondition extends ConditionBlock {
  IdentifierName: string = 'VendorClosed';
  Summary: string = "Vendor was closed";
  Description: string = "Evaluates to true if the vendor window was closed.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}