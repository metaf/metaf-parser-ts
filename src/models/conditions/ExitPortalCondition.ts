import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class ExitPortalCondition extends ConditionBlock {
  IdentifierName: string = 'ExitPortal';
  Summary: string = "Exited portal space";
  Description: string = "Evaluates to true when your character exits portal space.";
  Examples: string[] = [
    
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}