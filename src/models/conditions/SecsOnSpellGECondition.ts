
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class SecsOnSpellGECondition extends ConditionBlock {
  IdentifierName: string = 'SecsOnSpellGE';
  Summary: string = "Check spell buff duration";
  Description: string = "Evaluates to true if your character has a buff with SpellId that has at least Seconds seconds left on it.";
  Examples: string[] = [
    "True if >=120 seconds remain on 'Incantation of Armor Self', which has a SpellID of 4291.\n~~(Execute a '/vt dumpspells' command in-game. The far left column of the file it creates is the\n~~SpellID column.)\nSecsOnSpellGE 120 4291"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "SpellId",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      CompletionType: MetafCompletionType.SpellId,
      Description: "The spell id of the enchantment on your character to check."
    },
    {
      Name: "Seconds",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The amount of seconds to compare to the amount of seconds left on a spell."
    }
  ] as BlockArgument[];

  public Spellid: number;
  public Seconds: number;
}