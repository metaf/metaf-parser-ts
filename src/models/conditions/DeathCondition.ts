
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class DeathCondition extends ConditionBlock {
  IdentifierName: string = "Death";
  Summary: string = "Character died";
  Description: string = "Evaluates to true if yout character dies";
  Examples: string[] = [
    "Death"
  ];
  Arguments: BlockArgument[] = [];
}