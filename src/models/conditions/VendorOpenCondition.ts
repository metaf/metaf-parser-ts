
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class VendorOpenCondition extends ConditionBlock {
  IdentifierName: string = 'VendorOpen';
  Summary: string = "Vendor was opened";
  Description: string = "Evaluates to true if the vendor window was opened.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}