import { Token } from "antlr4ng";
import ConditionBlock from "./ConditionBlock";
import BlockArgument from "../BlockArgument";
import { ArgContext } from "../../gen/MetaFParser";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import { MetaFLexer } from "../../gen/MetaFlexer";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class BlockECondition extends ConditionBlock {
  IdentifierName: string = "BlockE";
  Summary: string = "Checks the current landblock";
  Description: string = "Evaluates to true if your characters current landblock is equal to the passed landblock.";
  Examples: string[] = [
    "~~True if leading 4 'digits' of character's @loc match leading 4 'digits'.\nBlockE 8B370000"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Landblock",
      ValidTokenTypes: [ MetaFLexer.HEX, MetaFLexer.NUMBER ],
      Type: MetafVarType.Hex,
      CompletionType: MetafCompletionType.LandblockId,
      Description: "The landblock to compare your character's position against. This should be a hex value in the form of `0xAAAA0000` where `AAAA` is the landblock id. You can get your current landblock by typing /loc ingame.",
      // Since metaf hex types aren't required to be prefixed the arg may be tokenized as a number if there
      // are only numeric digits, so we override the default arg value resolver here to account for that.
      ValueResolver: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
        if (token.text.trim().replace(/^0x/,'').length != 8) {
          visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing landblock '${token.text}': A hexadecimal value with 8 digits is expected in the format of \`0xAAAA0000\` where \`AAAA\` is the landblock id.`);
          return 0;
        }
        try {
          return parseInt(token.text, 16)
        }
        catch (e) {
          visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing landblock '${token.text}' as hexadecimal: ${e.message}`);
          return 0;
        }
      }
    }
  ] as BlockArgument[];

  public Landblock: number | undefined;
  public Test: string;
}