
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class NeverCondition extends ConditionBlock {
  IdentifierName: string = 'Never';
  Summary: string = "Never true";
  Description: string = "Always evaluates to false.";
  Examples: string[] = [
   "~~Never true\nNever"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}