import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class NotCondition extends ConditionBlock {
  IdentifierName: string = 'Not';
  Summary: string = "Negate next condition";
  Description: string = "Evaluates the specified condition and then returns the opposite.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];

  public Condition: ConditionBlock | undefined;
}