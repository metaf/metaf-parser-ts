import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class DistToRteGECondition extends ConditionBlock {
  IdentifierName: string = 'DistToRteGE';
  Summary: string = "Distance to route";
  Description: string = "Evaluates to true if the distance to the closest waypoint in the current nav is greater than Distance yards.";
  Examples: string[] = [
    "~~True when character exceeds 30 yard distance from current navroute.\nDistToRteGE 30"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Distance",
      ValidTokenTypes: [ MetaFLexer.NUMBER, MetaFLexer.DECIMAL ],
      Type: MetafVarType.Decimal,
      Description: "The distance in yards to check against all waypoints in the currently loaded nav."
    }
  ] as BlockArgument[];

  public Distance: number;
}