import ConditionBlock from "./ConditionBlock";
import MultiConditionBase from "./MultiConditionBase";
import BlockArgument from "../BlockArgument";

export default class AnyCondition extends MultiConditionBase {
  IdentifierName: string = 'Any';
  Summary: string = "true if any child conditions are true.";
  Description: string = "Evaluates to true if any child condition evaluates to true.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [];
}