import ConditionBlock from "./ConditionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import { ArgContext } from "../../gen/MetaFParser";
import { Token } from "antlr4ng";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class CellECondition extends ConditionBlock {
  IdentifierName: string = 'CellE';
  Summary: string = "Checks the current landcell";
  Description: string = "Evaluates to true if your characters current landcell is equal to the passed landcell.";
  Examples: string[] = [
    "~~True if all 8 'digits' of character's @loc match all 8 'digits'.\nBlockE 8B37E3A1"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Landcell",
      ValidTokenTypes: [ MetaFLexer.HEX, MetaFLexer.NUMBER ],
      Type: MetafVarType.Hex,
      CompletionType: MetafCompletionType.LandcellId,
      Description: "The landcell to compare your character's position against. This should be a hex value in the form of 0xAAAABBBB where AAAABBBB is the landcell id. You can get your current landcell by typing /loc ingame.",
      // Since metaf hex types aren't required to be prefixed the arg may be tokenized as a number if there
      // are only numeric digits, so we override the default arg value resolver here to account for that.
      ValueResolver: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
        if (token.text.trim().replace(/^0x/,'').length != 8) {
          visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing landcell '${token.text}': A hexadecimal value with 8 digits is expected in the format of \`0xAAAABBBB\` where \`AAAABBBB\` is the full landcell id.`);
          return 0;
        }
        try {
          return parseInt(token.text, 16)
        }
        catch (e) {
          visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing landcell '${token.text}' as hexadecimal: ${e.message}`);
          return 0;
        }
      }
    }
  ] as BlockArgument[];

  public Landcell: number | undefined;
}