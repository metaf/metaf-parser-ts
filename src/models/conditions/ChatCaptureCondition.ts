import ConditionBlock from "./ConditionBlock";
import BlockArgument from "../BlockArgument";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class ChatCaptureCondition extends ConditionBlock {
  IdentifierName: string = 'ChatCapture';
  Summary: string = "Chat text was captured";
  Description: string = `Evaluates to true if an incoming chat window message matched the specified pattern and color ids.\n#### Colors:
![chat colors](https://i.imgur.com/nMltZwV.png)
`;
  Examples: string[] = [
    "ChatCapture {^.*(?<who>Eskarina).* (says|tells you), \".+\"$} {2;4}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Pattern",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The regular expression to check against incoming chat messages. Capture groups are supported and will be set to expression variables."
    },
    {
      Name: "ColorIds",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "A list of semicolon separated chat color ids to limit matching to. If left blank, all chat color ids are matched.",
      DefaultValue: '',
      CompletionType: MetafCompletionType.ChatColor
    }
  ] as BlockArgument[];

  public Pattern: string | undefined;
  public ColorIds: string | undefined;
}