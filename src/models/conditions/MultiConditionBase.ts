import ConditionBlock from "./ConditionBlock";
import BlockArgument from "../BlockArgument";
import { ConditionBlockContext } from "../../gen/MetaFParser";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import { ParserModelBase } from "../ParserModelBase";

export default abstract class MultiConditionBase extends ConditionBlock {
  Children: ConditionBlock[] = [];
  ValidChildren = [ ...MetafModelBuilderVisitor.AvailableConditionBlocks ];

  constructor(ctx: ConditionBlockContext, parent: ParserModelBase | undefined, children: ConditionBlock[]=[]) {
    super(ctx, parent);
    this.Children = children || [];
  }
}