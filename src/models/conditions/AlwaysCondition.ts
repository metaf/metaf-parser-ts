import ConditionBlock from "./ConditionBlock";
import BlockArgument from "../BlockArgument";

export default class AlwaysCondition extends ConditionBlock {
  IdentifierName: string = 'Always';
  Summary: string = "Always evaluates to true.";
  Description: string = "Always evaluates to true.";
  Examples: string[] = [
    "~~ Always true\nAlways"
  ];
  Arguments: BlockArgument[] = [];
}