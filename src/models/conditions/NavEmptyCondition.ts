
import BlockArgument from "../BlockArgument";
import ConditionBlock from "./ConditionBlock";

export default class NavEmptyCondition extends ConditionBlock {
  IdentifierName: string = 'NavEmpty';
  Summary: string = "Check current nav is empty";
  Description: string = "Evaluates to true if the currently loaded VTank navigation route is empty.";
  Examples: string[] = [
   "~~True when the current navroute is empty.\nNavEmpty"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}