
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class NoMobsInDistCondition extends ConditionBlock {
  IdentifierName: string = 'NoMobsInDist';
  Summary: string = "Check no nearby mobs";
  Description: string = "Evaluates to true if there are no mobs within MobDistance yards of your character.";
  Examples: string[] = [
   "~~True if there are no mobs within 20 yards of your character.\nNoMobsInDist 20"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "MobDistance",
      ValidTokenTypes: [MetaFLexer.NUMBER, MetaFLexer.DECIMAL],
      Type: MetafVarType.Decimal,
      Description: "The distance in yards to check for mobs"
    }
  ] as BlockArgument[];

  public MobDistance: number;
}