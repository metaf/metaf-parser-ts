
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class ExprCondition extends ConditionBlock {
  IdentifierName: string = 'Expr';
  Summary: string = "Evaluate an expression";
  Description: string = "Evaluates to true when the specified expression evaluates to true.";
  Examples: string[] = [
    "~~True if variable myvar is an object type.\nExpr {7==getobjectinternaltype[getvar[myvar]]}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Expression",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyBlock,
      Description: "The expression to evaluate for truthiness."
    }
  ] as BlockArgument[];

  public Expression: string;
}