
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ConditionBlock from "./ConditionBlock";

export default class SecsInStateGECondition extends ConditionBlock {
  IdentifierName: string = 'SecsInStateGE';
  Summary: string = "Check time in state";
  Description: string = "Evaluates to true if you have been in this state more than Seconds seconds. Stopping the meta or changing states will reset this counter.";
  Examples: string[] = [
    "~~True 12 seconds after entering (and staying in) the rule's state, so long as the meta has\n~~been running the whole time. (It resets the timer counter to zero if the meta is turned off\n~~and back on, as if it's just entered the state.)\nSecsInStateGE 12"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Seconds",
      ValidTokenTypes: [MetaFLexer.NUMBER],
      Type: MetafVarType.Number,
      Description: "The amount of seconds in this state until this condition triggers. Switching states and pausing the meta restart this counter.",
    }
  ] as BlockArgument[];

  public Seconds: number;
}