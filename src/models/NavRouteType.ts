enum NavRouteType {
  Circular = 1,
  Follow,
  Linear,
  Once
}

export default NavRouteType;