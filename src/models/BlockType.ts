enum BlockType {
  Action,
  Condition,
  Waypoint,
  Meta,
  Nav,
  State,
  If,
  Do,
  Argument
}

export default BlockType;