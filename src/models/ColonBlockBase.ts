
import { ParserRuleContext } from "antlr4ng";
import { ParserModelBase } from "./ParserModelBase";

export abstract class ColonBlockBase extends ParserModelBase {
  constructor(ctx: ParserRuleContext, parent: ParserModelBase | undefined) {
    super(ctx, parent);
  }
}