enum MetafVarType {
  CurlyString,
  CurlyBlock,
  Number,
  Decimal,
  Hex,
  Identifier
}

export default MetafVarType;