import { ParserRuleContext } from "antlr4ng";
import BlockArgument from "./BlockArgument";
import MetafModelBuilderVisitor from "../visitors/MetafModelBuilderVisitor";

export abstract class ParserModelBase {
  abstract readonly IdentifierName: string;
  abstract readonly Arguments: BlockArgument[];
  abstract readonly ValidChildren: (typeof ParserModelBase)[];
  abstract readonly Summary: string;
  abstract readonly Description: string;
  abstract readonly Examples: string[];

  public Context: ParserRuleContext | undefined;
  public Deprecated: Boolean = false;
  public DeprecatedMessage: string = "Deprecated!";

  public Children: ParserModelBase[] = [];
  public ArgumentContexts: ParserRuleContext[] = [];
  public Parent: ParserModelBase;

  constructor(ctx: ParserRuleContext | undefined, parent: ParserModelBase | undefined) {
    this.Context = ctx;
    this.Parent = parent;
  }

  validate(visitor: MetafModelBuilderVisitor) {

  }

  getMarkdownDocs() : string {
    let docs = `### ${this.IdentifierName}\n` +  this.Description;

    if (this.Arguments && this.Arguments.length > 0) {
      docs += `\n#### Arguments:\n`;

      let i = 1;
      for (const argument of this.Arguments) {
        docs += ` - #${i++} ${argument.Name}: ${argument.Description}\n`
      }
    }

    if (this.Examples && this.Examples.length > 0) {
      docs += `\n#### Examples:\n`;

      for (const example of this.Examples) {
        docs += "```metaf\n" + example + "\n```\n"
      }
    }

    return docs;
  }
}