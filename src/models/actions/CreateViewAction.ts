import ActionBlock from "./ActionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";

export default class CreateViewAction extends ActionBlock {
  IdentifierName: string = 'CreateView';
  Summary: string = "Evaluate an expression and send the resulting text to the chat window.";
  Description: string = "Evaluates an expression and sends the resulting text or command to the chat window, as if you had typed it in.";
  Examples: string[] = [
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "ViewHandle",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The view handle to use for this view. This should be a unique id for this view xml."
    },
    {
      Name: "ViewXML",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyBlock,
      Description: "The XML that defines this view."
    }
  ] as BlockArgument[];

  public ViewHandle: string;
  public ViewXML: string;
}