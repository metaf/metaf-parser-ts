import ActionBlock from "./ActionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class SetOptAction extends ActionBlock {
  IdentifierName: string = 'SetOpt';
  Summary: string = "Set a VTank option";
  Description: string = "Set the specified VTank Option to the value returned by evaluating the passed expression.";
  Examples: string[] = [
    "~~ Sets the VirindiTank 'OpenDoors' option to true if any doors are nearby, false otherwise.\nSetOpt {OpenDoors} {istrue[wobjectfindnearestdoor[]]}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "SettingName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.VTankOpt,
      Description: "The name of the VTank setting to set."
    },
    {
      Name: "Expression",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyBlock,
      Description: "The expression to evaluate. The specified setting will be set to the result."
    }
  ] as BlockArgument[];

  public SettingName: string;
  public Expression: string;
}