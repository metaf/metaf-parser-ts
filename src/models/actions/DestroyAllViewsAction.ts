import BlockArgument from "../BlockArgument";
import ActionBlock from "./ActionBlock";

export default class DestroyAllViewsAction extends ActionBlock {
  IdentifierName: string = 'DestroyAllViews';
  Summary: string = "Destroy all meta views";
  Description: string = "Destroys all views created by this meta";
  Examples: string[] = [
    "~~Destroy all meta views.\nDestroyAllViews"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}