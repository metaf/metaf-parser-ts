import BlockArgument from "../BlockArgument";
import ActionBlock from "./ActionBlock";

export default class ClearWatchdogAction extends ActionBlock {
  IdentifierName: string = 'ClearWatchdog';
  Summary: string = "Clear the current watchdog";
  Description: string = "Clear the current watchdog in this state.";
  Examples: string[] = [
    "~~Clears the watchdog.\nClearWatchdog"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}