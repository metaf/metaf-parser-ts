import ActionBlock from "./ActionBlock";
import BlockArgument from "../BlockArgument";
import { MetaFLexer } from "../../gen/MetaFlexer";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";

export default class EmbedNavAction extends ActionBlock {
  IdentifierName: string = 'EmbedNav';
  Summary: string = "Embed a nav route";
  Description: string = "Embeds a nav route with the specified id.";
  Examples: string[] = [
    "~~Embed a nav route with the id 'nav1'.\nEmbedNav nav1 {}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "NavId",
      ValidTokenTypes: [ MetaFLexer.IDENTIFIER ],
      CompletionType: MetafCompletionType.NavRef,
      Type: MetafVarType.Identifier,
      Description: "The id of the navigation file to embed."
    },
    {
      Name: "NavFileName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The original filename of the embedded nav."
    },
    {
      Name: "NavTransform",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The transformation to apply to all the waypoints in this nav.",
      ValueResolver(token, ctx, parser) {
        const parts = token.text.trim().split(/\s+/);
        if (parts.length != 7) {
          parser.reportError(ctx, MetafErrorType.InvalidArgumentCount, `NavTransform expects exactly 7 numbers/decimals to be passed. ie: {1 0 0 1 0 0 0}`);
          return new NavTransform(1, 0, 0, 1, 0, 0, 0);
        }
        
        const args: number[] = [];
        for (const num of parts) {
          try {
              args.push(parseFloat(num))
          }
          catch (e) {
            this.reportError(ctx, `Unable to parse float '${num}': ${e.message}`);
            return new NavTransform(1, 0, 0, 1, 0, 0, 0);
          }
        }

        return new NavTransform(args[0], args[1], args[2], args[3], args[4], args[5], args[6]);
      },
      DefaultValue: () => {
        return new NavTransform(1, 0, 0, 1, 0, 0, 0)
      }
    }
  ] as BlockArgument[];

  public NavId: string;
  public NavFileName: string;
  public NavTransform: NavTransform;
}

export class NavTransform {
  public A: number;
  public B: number;
  public C: number;
  public D: number;
  public E: number;
  public F: number;
  public G: number;

  constructor(a: number, b: number, c: number, d: number, e: number, f: number, g: number) {
    this.A = a;
    this.B = b;
    this.C = c;
    this.D = d;
    this.E = e;
    this.F = f;
    this.G = g;
  }
}