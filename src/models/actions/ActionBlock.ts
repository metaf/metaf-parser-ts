
import { ActionBlockContext } from "../../gen/MetaFParser";
import BlockArgument from "../BlockArgument";
import { BlockBase } from "../BlockBase";
import { ParserModelBase } from "../ParserModelBase";

export default abstract class ActionBlock extends BlockBase {
  abstract IdentifierName: string;
  abstract Arguments: BlockArgument[];
  readonly ValidChildren: (typeof ParserModelBase)[] = [];

  constructor(ctx: ActionBlockContext, parent: ParserModelBase | undefined) {
    super(ctx, parent);
  }
}