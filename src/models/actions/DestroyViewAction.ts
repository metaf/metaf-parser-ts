import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";
import ActionBlock from "./ActionBlock";

export default class DestroyViewAction extends ActionBlock {
  IdentifierName: string = 'DestroyView';
  Summary: string = "Destroy a meta view";
  Description: string = "Destroy the meta view with the specified handle";
  Examples: string[] = [
    "~~Destroy all meta views.\nDestroyAllViews"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "ViewHandle",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.ViewRef,
      Description: "The view handle to use for this view. This should be a unique id for this view xml."
    }
  ] as BlockArgument[];

  public ViewHandle: string;
}