import ActionBlock from "./ActionBlock";
import BlockArgument from "../BlockArgument";

export default class ReturnAction extends ActionBlock {
  IdentifierName: string = 'Return';
  Summary: string = "Return to the last state pushed to the return stack.";
  Description: string = "Retusn to the last state that was pushed to the return stack using CallState";
  Examples: string[] = [
    "~~ return to the last state on the return stack\nReturn"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}