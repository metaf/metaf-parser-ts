import ActionBlock from "./ActionBlock";
import BlockArgument from "../BlockArgument";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";

export default class CallStateAction extends ActionBlock {
  IdentifierName: string = 'CallState';
  Summary: string = "Call into another state and add a state to the return stack.";
  Description: string = "Calls into another state and adds a state to the return stack.";
  Examples: string[] = [
    "~~Call into state `Recomp` and add `Hunting` to the return stack.\nCallState {Recomp} {Hunting}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "NewState",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      CompletionType: MetafCompletionType.StateRef,
      Type: MetafVarType.CurlyString,
      Description: "The state to transition to."
    },
    {
      Name: "ReturnState",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      CompletionType: MetafCompletionType.StateRef,
      Type: MetafVarType.CurlyString,
      Description: "The state to add to the return stack."
    } as BlockArgument
  ] as BlockArgument[];

  public NewState: string;
  public ReturnState: string;
}