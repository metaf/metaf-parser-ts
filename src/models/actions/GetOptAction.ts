import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";
import ActionBlock from "./ActionBlock";

export default class GetOptAction extends ActionBlock {
  IdentifierName: string = 'GetOpt';
  Summary: string = "Get a vtank option";
  Description: string = "Get the specified VTank option value and set it to an expression variable of the specified name.";
  Examples: string[] = [
    "~~ Gets current status of the 'OpenDoors' VirindiTank option, and stores it in expression variable 'doors'\nGetOpt {OpenDoors} {doors}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "SettingName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.VTankOpt,
      Description: "The name of the VTank setting to get."
    },
    {
      Name: "VariableName",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The name of the expression variable to save the resulting setting value to."
    }
  ] as BlockArgument[];

  public SettingName: string;
  public VariableName: string;
}