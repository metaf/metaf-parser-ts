import ActionBlock from "./ActionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";

export default class ChatAction extends ActionBlock {
  IdentifierName: string = 'Chat';
  Summary: string = "Send a message/command to the chat window.";
  Description: string = "Sends a message or command to the chat window, as if you had typed it in.";
  Examples: string[] = [
    "~~Say 'hello world' in local chat.\nChat {/say hello world}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Message",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The message / command to send to the chat window."
    }
  ] as BlockArgument[];

  public Message: string;
}