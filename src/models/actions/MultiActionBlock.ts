import { ActionBlockContext } from "../../gen/MetaFParser";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import { ParserModelBase } from "../ParserModelBase";
import ActionBlock from "./ActionBlock";

export default abstract class MultiActionBlock extends ActionBlock {
  public Children: ActionBlock[] = [];
  ValidChildren = [ ...MetafModelBuilderVisitor.AvailableActionBlocks ];

  constructor(ctx: ActionBlockContext, parent: ParserModelBase | undefined, children: ActionBlock[]=[]) {
    super(ctx, parent);
    this.Children = children || [];
  }
}