import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ActionBlock from "./ActionBlock";

export default class ChatExprAction extends ActionBlock {
  IdentifierName: string = 'ChatExpr';
  Summary: string = "Evaluate an expression and send the resulting text to the chat window.";
  Description: string = "Evaluates an expression and sends the resulting text or command to the chat window, as if you had typed it in.";
  Examples: string[] = [
    "~~Say 'hello my name is <character_name>' in local chat.\nChatExpr {`hello my name is `+getcharstringprop[1]}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Expression",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyBlock
    }
  ] as BlockArgument[];

  public Expression: string;
}