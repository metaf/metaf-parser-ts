import ActionBlock from "./ActionBlock";
import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";

export default class SetStateAction extends ActionBlock {
  IdentifierName: string = 'SetState';
  Summary: string = "Change to the specified meta state.";
  Description: string = "Sets the current meta state to the passed state.";
  Examples: string[] = [
    "~~ Transition to state 'Target Name'.\nSetState {Target Name}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "NewState",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.StateRef,
      Description: "The meta state to transition to"
    }
  ] as BlockArgument[];

  public NewState: string;
}