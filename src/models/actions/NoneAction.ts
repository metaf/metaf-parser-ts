import ActionBlock from "./ActionBlock";
import BlockArgument from "../BlockArgument";

export default class NoneAction extends ActionBlock {
  IdentifierName: string = 'None';
  Summary: string = "Does nothing";
  Description: string = "Does nothing.";
  Examples: string[] = [
    "~~ Do nothing\nNone"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}