import ActionBlock from "./ActionBlock";
import BlockArgument from "../BlockArgument";
import { MetaFLexer } from "../../gen/MetaFlexer";
import MetafCompletionType from "../MetafCompletionType";
import MetafVarType from "../MetafVarType";

export default class SetWatchdogAction extends ActionBlock {
  IdentifierName: string = 'SetWatchdog';
  Summary: string = "Set a watchdog for this state.";
  Description: string = "Sets a watchdog that monitors your characters movement and changes state if no movement of the specified distance in the specified time has taken place.";
  Examples: string[] = [
    "~~If at some point while in the current state your character hasn't moved at least 12.3 yards in the preceding 4.6 seconds, state 'Oh, no!' is called.\nSetWatchdog 12.3 4.6 {Oh, no!}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Distance",
      ValidTokenTypes: [ MetaFLexer.NUMBER, MetaFLexer.DECIMAL ],
      Type: MetafVarType.Decimal,
      Description: "The minimum distance the character needs to move within Seconds before this watchdog is triggered."
    },
    {
      Name: "Seconds",
      ValidTokenTypes: [ MetaFLexer.NUMBER ],
      Type: MetafVarType.Number,
      Description: "The amount of seconds without moving Distance before this watchdog is triggered."
    },
    {
      Name: "NewState",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      CompletionType: MetafCompletionType.StateRef,
      Description: "The name of the state to switch to."
    }
  ] as BlockArgument[];

  public Distance: number;
  public Seconds: number;
  public NewState: string
}