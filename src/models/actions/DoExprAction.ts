import { MetaFLexer } from "../../gen/MetaFlexer";
import BlockArgument from "../BlockArgument";
import MetafVarType from "../MetafVarType";
import ActionBlock from "./ActionBlock";

export default class DoExprAction extends ActionBlock {
  IdentifierName: string = 'DoExpr';
  Summary: string = "Evaluate an expression";
  Description: string = "Evaluates the specified expression.";
  Examples: string[] = [
    "~~Evaluates an echo[] expression.\nDoExpr {echo[`Hello`]}"
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Expression",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyBlock,
      Description: "The expression to evaluate."
    }
  ] as BlockArgument[];

  public Expression: string;
}