import MultiActionBlock from "./MultiActionBlock";
import ActionBlock from "./ActionBlock";
import { ParserRuleContext } from "antlr4ng";
import BlockArgument from "../BlockArgument";
import NoneAction from "./NoneAction";

export default class DoAllAction extends MultiActionBlock {
  IdentifierName: string = 'DoAll';
  Summary: string = "Do all of the specified child actions";
  Description: string = "Do all of the specified child actions";
  Examples: string[] = [
    "~~Send a message to chat and then switch to the 'Default' state.\nDoAll\n\tChat {Hello}\n\tSetState {Default}"
  ];
  Arguments: BlockArgument[] = [] as BlockArgument[];
}