import BlockArgument from "./BlockArgument";
import NavRoute from "./blocks/NavBlock";
import { ParserModelBase } from "./ParserModelBase";
import State from "./blocks/StateBlock";
import NavBlock from "./blocks/NavBlock";

export class Meta extends ParserModelBase {
  IdentifierName: string = "Meta";
  Summary: string = "TODO";
  Description: string = "TODO";
  Examples: string[] = [
   "~~TODO"
  ];
  Arguments: BlockArgument[] = [];
  ValidChildren = [ NavBlock ];

  public Name: string = '';
  public States: State[] = [];
  public NavRoutes: NavRoute[] = [];


  printTree() {
    let depth = 0;
    console.log(`Meta: ${this.Name}`);
    depth++;

    console.log(`${'\t'.repeat(depth)}States:`);
    depth++;
    for (const state of this.States) {
      this.printChild(state, depth);
    }
    depth--;

    console.log(`${'\t'.repeat(depth)}NavRoutes:`);
    depth++;
    for (const navRoute of this.NavRoutes) {
      this.printChild(navRoute, depth);
    }
    depth--;
  }

  printChild(model: ParserModelBase, depth: number) {
    console.log(`${'\t'.repeat(depth)}${(model['IsNotPrefixed'] ? 'NOT ' : '')}${model.IdentifierName}`);
    for (const key of Object.keys(model)) {
      if (key === 'Parent') {
        continue;
      }
      //console.log(`${'\t'.repeat(depth+1)}LookAt: ${key}: ${model[key] instanceof ParserModelBase} // ${Array.isArray(model[key])} && ${model[key].length > 0} && ${model[key][0] instanceof ParserModelBase}`)
      if (model[key] instanceof ParserModelBase) {
        console.log(`${'\t'.repeat(depth + 1)}${key}:`);
        this.printChild(model[key], depth + 2);
      }
      else if (Array.isArray(model[key]) && model[key].length > 0 && model[key][0] instanceof ParserModelBase) {
        console.log(`${'\t'.repeat(depth + 1)}${key}[]:`);
        for (const i of model[key]) {
          this.printChild(i, depth + 2);
        }
      }
    }
  }
}