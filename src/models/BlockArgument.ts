import { ParserRuleContext, Token } from "antlr4ng";
import MetafModelBuilderVisitor from "../visitors/MetafModelBuilderVisitor";
import { ArgContext } from "../gen/MetaFParser";
import { BlockBase } from "./BlockBase";
import { ParserModelBase } from "./ParserModelBase";
import MetafCompletionType from "./MetafCompletionType";
import MetafVarType from "./MetafVarType";

export default interface BlockArgument {
  ValidTokenTypes: number[] | undefined,
  ValidChildBlocks: (typeof BlockBase)[] | undefined,
  CompletionType?: MetafCompletionType,
  Name: string,
  Type: MetafVarType,
  Description: string,
  DefaultValue: any | undefined,
  Context: ParserRuleContext | undefined,
  ValueResolver: undefined | ((token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => any) | ((token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor, model: ParserModelBase, args: ArgContext[], argIndex: number) => any);
}

