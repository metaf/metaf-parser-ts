import { Token } from "antlr4ng";
import { ArgContext } from "../../gen/MetaFParser";
import { MetaFLexer } from "../../gen/MetaFlexer";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import NavRouteType from "../NavRouteType";
import { ColonBlockBase } from "../ColonBlockBase";
import WaypointBlock from "../navnodes/WaypointBlock";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class NavBlock extends ColonBlockBase {
  IdentifierName: string = "NAV";
  Summary: string = "Define a nav route";
  Description: string = "Define a nav route";
  Examples: string[] = [];
  ValidChildren = [
    ...MetafModelBuilderVisitor.AvailableWaypointBlocks
  ];
  Arguments: BlockArgument[] = [
    {
      Name: "Id",
      ValidTokenTypes: [ MetaFLexer.IDENTIFIER ],
      Type: MetafVarType.Identifier,
      Description: "The id of this NavRoute, used to refer to it throughout the meta.",
      CompletionType: MetafCompletionType.NavDef
    },
    {
      Name: "Type",
      ValidTokenTypes: [ MetaFLexer.IDENTIFIER ],
      Description: "The type of NavRoute. Valid types are: 'circular' | 'follow' | 'linear' | 'once'.",
      Type: MetafVarType.Identifier,
      CompletionType: MetafCompletionType.NavRouteType,
      ValueResolver: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
        const typeText = ctx.getText().trim().toLowerCase();
    
        switch (typeText) {
          case 'circular':
            return NavRouteType.Circular;
          case 'follow':
            return NavRouteType.Follow;
          case 'linear':
            return NavRouteType.Linear;
          case 'once':
            return NavRouteType.Once;
        }

        visitor.reportError(ctx, MetafErrorType.UnexpectedIdentifier, `Invalid NavRouteType: ${ctx.getText().trim()}: Should be one of 'circular' | 'follow' | 'linear' | 'once'.`);
    
        return NavRouteType.Circular;
      }
    }
  ] as BlockArgument[];

  public Id: string;
  public Type: NavRouteType;
  public Waypoints: WaypointBlock[] = [];
}