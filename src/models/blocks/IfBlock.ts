import { Token } from "antlr4ng";
import { MetaFLexer } from "../../gen/MetaFlexer";
import TokenValueResolvers from "../../lib/TokenValueResolvers";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import { BlockBase } from "../BlockBase";
import { ColonBlockBase } from "../ColonBlockBase";
import ConditionBlock from "../conditions/ConditionBlock";
import DoBlock from "./DoBlock";

export default class IfBlock extends ColonBlockBase {
  IdentifierName: string = "IF";
  Summary: string = "Check the specified conditions";
  Description: string = "Check the specified conditions";
  Examples: string[] = [];
  Arguments: BlockArgument[] = [] as BlockArgument[];
  ValidChildren = [ DoBlock ];
  
  public Condition: ConditionBlock;
  public Do: DoBlock;
}