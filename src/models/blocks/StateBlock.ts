import BlockArgument from "../BlockArgument";
import { MetaFLexer } from "../../gen/MetaFlexer";
import { ColonBlockBase } from "../ColonBlockBase";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import { MetafErrorType } from "../../lib/MetafErrorListener";
import IfBlock from "./IfBlock";
import { StateBlockContext } from "../../gen/MetaFParser";
import { ParserModelBase } from "../ParserModelBase";
import MetafVarType from "../MetafVarType";
import MetafCompletionType from "../MetafCompletionType";

export default class StateBlock extends ColonBlockBase {
  readonly IdentifierName: string = "STATE";
  Summary: string = "Define a state";
  Description: string = "Define a state";
  Examples: string[] = [
`~~ A state that says 'Hello World' in chat when it is entered.
STATE: {Default}
  IF: Always
    DO: Chat {/say Hello World}`
  ];
  readonly ValidChildren = [ IfBlock ];
  readonly Arguments = [
    {
      Name: "Id",
      ValidTokenTypes: [ MetaFLexer.CURLYCONTENTS ],
      Type: MetafVarType.CurlyString,
      Description: "The id/name of this state, used to refer to it throughout the meta.",
      CompletionType: MetafCompletionType.StateDef
    }
  ] as BlockArgument[];
  
  public Id: string;
  public Rules: IfBlock[] = [];

  validate(visitor: MetafModelBuilderVisitor) {
    if (this.Rules.length == 0) {
      visitor.reportError(this.Context, MetafErrorType.Error, 'States must contain at least one rule!')
    }

    let wasIFirst = false;
    for (const state of visitor.Meta.States) {
      if (this == state) {
        wasIFirst = true;
      }
      else if (state.Id == this.Id && !wasIFirst) {
        visitor.reportError(this.Context, MetafErrorType.DuplicateDefinition, `A state with the name '${state.Id}' was already defined!`);
        break;
      }
    }
  }
}