import { MetaFLexer } from "../../gen/MetaFlexer";
import TokenValueResolvers from "../../lib/TokenValueResolvers";
import MetafModelBuilderVisitor from "../../visitors/MetafModelBuilderVisitor";
import BlockArgument from "../BlockArgument";
import { BlockBase } from "../BlockBase";
import { ColonBlockBase } from "../ColonBlockBase";
import { ParserModelBase } from "../ParserModelBase";
import { DoAllAction, CallStateAction, ChatAction, ChatExprAction, ClearWatchdogAction, CreateViewAction, DestroyAllViewsAction, DestroyViewAction, DoExprAction, EmbedNavAction, GetOptAction, NoneAction, ReturnAction, SetOptAction, SetStateAction, SetWatchdogAction } from "../actions";
import ActionBlock from "../actions/ActionBlock";
import ConditionBlock from "../conditions/ConditionBlock";

export default class DoBlock extends ColonBlockBase {
  IdentifierName: string = "DO";
  Summary: string = "Do the specified actions";
  Description: string = "Do the specified actions";
  Examples: string[] = [];
  Arguments: BlockArgument[] = [] as BlockArgument[];
  ValidChildren = [ ];
  
  public Action: ActionBlock;
}