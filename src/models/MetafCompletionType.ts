enum MetafCompletionType {
  ItemName,
  LandblockId,
  LandcellId,
  StateRef,
  ViewRef,
  NavRef,
  VTankOpt,
  SpellId,
  NavRouteType,
  RecallSpellName,
  ObjectClass,
  ChatColor,
  StateDef,
  NavDef
}

export default MetafCompletionType;