
import { Token } from "antlr4ng";
import { ArgContext, ArgsListContext } from "../gen/MetaFParser";
import MetafModelBuilderVisitor from "../visitors/MetafModelBuilderVisitor";
import { MetafErrorType } from "./MetafErrorListener";
import { ObjectClass } from "./ObjectClass";

export default {
  resolveBoolean: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
    const value = token.text.trim().toLowerCase();
    switch (value) {
      case 'true':
        return true;
      case 'false':
        return false;
      default:
        visitor.reportError(ctx, MetafErrorType.ParseError, `Unable to parse boolean '${token.text}', expected 'True' | 'False'.`);
        return false;
    }
  },

  resolveObjectId: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
    if (token.text.trim().replace(/^0x/,'').length != 8) {
      visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing ObjectId '${token.text}': A hexadecimal value with 8 digits is expected in the format of \`0xAAAAAAAA\`.`);
      return 0;
    }
    try {
      return parseInt(token.text, 16)
    }
    catch (e) {
      visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing ObjectId '${token.text}' as hexadecimal: ${e.message}`);
      return 0;
    }
  },

  resolveObjectClass: (token: Token, ctx: ArgContext, visitor: MetafModelBuilderVisitor) => {
    try {
      const parsedValue = parseInt(token.text);

      if (ObjectClass[parsedValue] == undefined) {
        visitor.reportError(ctx, MetafErrorType.InvalidArgumentType, `Error parsing ObjectClass '${token.text}': Unknown ObjectClass.`)
      }
      return parsedValue;
    }
    catch (e) {
      visitor.reportError(ctx, MetafErrorType.ParseError, `Error parsing ObjectClass '${token.text}': ${e.message}`);
      return 0;
    }
  }
}