export default {
  "Types": {
      "CurlyString": {
        "description": "A string of characters surrounded by curly braces. To use curly braces inside your string, double them up like {{. These may not contain newline characters.",
        "examples": [
          {
            "example": "{this is a curly string}",
            "description": "A curly string with the text 'this is a curly string'."
          }
        ] 
      },
      "CurlyBlock": {
        "description": "A string of characters surrounded by curly braces. The initial opening curly brace must be followed by a newline character, and the final closing brace should be on its own line. These can include newlines and do not need to have inner curly braces escaped.",
        "examples": [
          {
            "example": "{\n\tThis is a\ncurly block\n}",
            "description": "A curly block with newline characters in the content"
          }
        ]
      },
      "RegexPattern": {
        "backingType": "CurlyString",
        "description": "A regular expression pattern surrounded by curly braces",
        "examples": [
          {
            "example": "{^test$}",
            "description": "A regular expression `^test$` that matches the word test exactly."
          }
        ]
      },
      "Number": {
        "description": "A whole number type, written in base 10 format",
        "examples": [
          {
            "example": "123",
            "description": "The number `123`"
          }
        ]
      },
      "Decimal": {
        "description": "A decimal number optionally written in scientific notation.",
        "examples": [
          {
            "example": "123.456",
            "description": "The decimal number `123.456`."
          }
        ]
      },
      "Identifier": {
        "description": "A literal identifier. These should start with a letter or underscore, and then only contain letters, underscores, and numbers.",
        "examples": [
          {
            "example": "nav1",
            "description": "A literal identifier with the value of `nav1`."
          }
        ]
      },
      "ChatCommand": {
        "backingType": "CurlyString",
        "description": "A command or message wrapped in curly braces that will be sent to the chat window, as if you had typed it in and hit enter.",
        "examples": [
          {
            "example": "{/my command}",
            "description": "The chat command `/my command`."
          }
        ]
      },
      "LandblockId": {
        "description": "An identifier representing a landblock in the game world.",
        "examples": [
          {
            "example": "1",
            "description": "A landblock identifier with the value `1`."
          }
        ]
      },
      "LandcellId": {
        "description": "An identifier representing a landcell in the game world.",
        "examples": [
          {
            "example": "123",
            "description": "A landcell identifier with the value `123`."
          }
        ]
      },
      "ViewId": {
        "description": "An identifier representing a view in the game UI.",
        "examples": [
          {
            "example": "inventory",
            "description": "A view identifier for the inventory UI."
          }
        ]
      },
      "VtOptionRef": {
        "description": "A reference to a Virtual Targeting option.",
        "examples": [
          {
            "example": "vt_option",
            "description": "A reference to the Virtual Targeting option with the name `vt_option`."
          }
        ]
      },
      "StateRef": {
        "description": "A reference to a game state.",
        "examples": [
          {
            "example": "idle",
            "description": "A reference to the game state `idle`."
          }
        ]
      },
      "NavRef": {
        "description": "A reference to a navigation marker in the game world.",
        "examples": [
          {
            "example": "nav_point",
            "description": "A reference to the navigation marker with the name `nav_point`."
          }
        ]
      },
      "XmlBlock": {
        "description": "An XML block representing UI structure.",
        "examples": [
          {
            "example": "<Button text='Click me' onClick='handleClick' />",
            "description": "An XML block defining a button with text and an onClick event."
          }
        ]
      }
  },
  "Actions": {
    "Chat": {
      "description": "Sends the passed text as an in-game chat command, as if you had typed it into the chat window.",
      "arguments": [
        {
          "type": "ChatCommand",
          "description": "The text to send to the chat window.",
        }
      ],
      "examples": [
        "~~ Say 'Hello World' in local chat\nChat {/say Hello World}"
      ]
    },
    "CallState": {
      "description": "Calls a specified game state.",
      "arguments": [
        {
          "type": "StateRef",
          "description": "The state to call."
        },
        {
          "type": "StateRef",
          "description": "The state to return to after the called state completes."
        }
      ],
      "examples": [
        "~~ Set the state to 'Recomp' and add the 'Hunt' state to the return stack.\nCallState {Recomp} {Hunt}"
      ]
    },
    "DoAll": {
      "description": "Performs all of the child actions",
      "examples": [
        "~~ Perform a Chat and CallState action with DoAll\nDO: DoAll\n\tChat {/mp}\n\tCallState {Recomp} {Hunt}"
      ]
    },
    "ChatExpr": {
      "description": "Sends the result of the provided expression as an in-game chat command.",
      "arguments": [
        {
          "type": "CurlyString",
          "description": "The expression to evaluate and send as a chat command."
        }
      ]
    },
    "DoExpr": {
      "description": "Executes the provided Lua expression.",
      "arguments": [
        {
          "type": "CurlyString",
          "description": "The Lua expression to execute."
        }
      ]
    },
    "EmbedNav": {
      "description": "Embeds a navigation marker in the game world with the specified name and transformation.",
      "arguments": [
        {
          "type": "NavRef",
          "description": "The navigation marker reference."
        },
        {
          "type": "CurlyString",
          "description": "The name of the navigation marker."
        },
        {
          "type": "CurlyBlock",
          "description": "The transformation to apply to the navigation marker."
        }
      ],
      "examples": [
        "~~ Embed a navroute with the id 'nav1' \nEmbedRoute nav1 {}"
      ]
    },
    "None": {
      "description": "Does nothing.",
      "arguments": [],
      "examples": [
        "~~ Do nothing... \nNone"
      ]
    },
    "Return": {
      "description": "Returns from the current action block.",
      "arguments": []
    },
    "SetState": {
      "description": "Sets the game state to the specified state.",
      "arguments": [
        {
          "type": "StateRef",
          "description": "The state to set."
        }
      ],
      "examples": [
        "~~ Set the current state to 'MyState' \nSetState {MyState}"
      ]
    },
    "SetWatchdog": {
      "description": "Sets a watchdog timer that, if not cleared within the specified time, triggers a specified state.",
      "arguments": [
        {
          "type": "Decimal",
          "description": "The distance to trigger the watchdog."
        },
        {
          "type": "Decimal",
          "description": "The time in seconds before triggering the watchdog."
        },
        {
          "type": "StateRef",
          "description": "The state to trigger if the watchdog timer is not cleared."
        }
      ]
    },
    "ClearWatchdog": {
      "description": "Clears the currently set watchdog timer.",
      "arguments": []
    },
    "GetOpt": {
      "description": "Gets the value of a Virtual Targeting option and assigns it to a variable.",
      "arguments": [
        {
          "type": "VtOptionRef",
          "description": "The Virtual Targeting option reference."
        },
        {
          "type": "CurlyString",
          "description": "The variable to assign the option value to."
        }
      ]
    },
    "SetOpt": {
      "description": "Sets the value of a Virtual Targeting option.",
      "arguments": [
        {
          "type": "VtOptionRef",
          "description": "The Virtual Targeting option reference."
        },
        {
          "type": "CurlyBlock",
          "description": "The expression block representing the new value for the option."
        }
      ]
    },
    "CreateView": {
      "description": "Creates a new UI view with the specified ID and XML structure.",
      "arguments": [
        {
          "type": "CurlyString",
          "description": "The ID of the new UI view."
        },
        {
          "type": "XmlBlock",
          "description": "The XML structure defining the UI view."
        }
      ]
    },
    "DestroyView": {
      "description": "Destroys the UI view with the specified reference.",
      "arguments": [
        {
          "type": "CurlyString",
          "description": "The reference to the UI view to destroy."
        }
      ]
    },
    "DestroyAllViews": {
      "description": "Destroys all UI views.",
      "arguments": []
    }
  },
  "Conditions": {
      "All": {
        "description": "Conditional block that evaluates to true if all nested conditions are true.",
        "arguments": [
          {
            "type": "ConditionBlock",
            "description": "The nested conditions to evaluate."
          }
        ]
      },
      "Always": {
        "description": "Conditional block that always evaluates to true.",
        "arguments": []
      },
      "Any": {
        "description": "Conditional block that evaluates to true if any nested condition is true.",
        "arguments": [
          {
            "type": "ConditionBlock",
            "description": "The nested conditions to evaluate."
          }
        ]
      },
      "BlockE": {
        "description": "Conditional block that evaluates to true if the specified land block is empty.",
        "arguments": [
          {
            "type": "LandBlockId",
            "description": "The ID of the land block to check."
          }
        ]
      },
      "BuPercentGE": {
        "description": "Conditional block that evaluates to true if the player's buff percentage is greater than or equal to the specified value.",
        "arguments": [
          {
            "type": "Number",
            "description": "The percentage value to compare."
          }
        ]
      },
      "CellE": {
        "description": "Conditional block that evaluates to true if the specified land cell is empty.",
        "arguments": [
          {
            "type": "LandCellId",
            "description": "The ID of the land cell to check."
          }
        ]
      },
      "ChatCapture": {
        "description": "Conditional block that evaluates to true if the chat window captures a message matching the specified pattern.",
        "arguments": [
          {
            "type": "RegexPattern",
            "description": "The regular expression pattern to match."
          },
          {
            "type": "Optional",
            "description": "Optional: Either a list of chat colors to capture or a list of chat colors to ignore."
          }
        ]
      },
      "ChatMatch": {
        "description": "Conditional block that evaluates to true if the chat window captures a message matching the specified pattern.",
        "arguments": [
          {
            "type": "RegexPattern",
            "description": "The regular expression pattern to match."
          }
        ],
        "examples": [
          '~~ match when anyone says "YourCommandHere" in chat\nChat {^.*().* (say|says|tells you), \"YourCommandHere\"$ }'
        ]
      },
      "Death": {
        "description": "Conditional block that evaluates to true if the player character dies.",
        "arguments": []
      },
      "DistanceToRteGE": {
        "description": "Conditional block that evaluates to true if the distance to the specified route is greater than or equal to the specified value.",
        "arguments": [
          {
            "type": "Decimal",
            "description": "The distance value to compare."
          }
        ]
      },
      "ExitPortal": {
        "description": "Conditional block that evaluates to true if the player character is at an exit portal.",
        "arguments": []
      },
      "Expr": {
        "description": "Conditional block that evaluates to true based on the result of the specified Lua expression.",
        "arguments": [
          {
            "type": "ExpressionBlock",
            "description": "The Lua expression to evaluate."
          }
        ]
      },
      "IntoPortal": {
        "description": "Conditional block that evaluates to true if the player character is at an into portal.",
        "arguments": []
      },
      "ItemCountGE": {
        "description": "Conditional block that evaluates to true if the player has at least the specified count of the specified item.",
        "arguments": [
          {
            "type": "Number",
            "description": "The item count value to compare."
          },
          {
            "type": "CurlyString",
            "description": "The name of the item to check."
          }
        ],
        "examples": [
          "~~ Evaluates to true if you have more than 5 Prismatic Tapers in your inventory\nItemCountGE {Prismatic Taper} 5"
        ]
      },
      "ItemCountLE": {
        "description": "Conditional block that evaluates to true if the player has at most the specified count of the specified item.",
        "arguments": [
          {
            "type": "Number",
            "description": "The item count value to compare."
          },
          {
            "type": "CurlyString",
            "description": "The name of the item to check."
          }
        ]
      },
      "MainSlotsLE": {
        "description": "Conditional block that evaluates to true if the number of main slots is less than or equal to the specified count.",
        "arguments": [
          {
            "type": "Number",
            "description": "The main slots count value to compare."
          }
        ],
        "examples": [
          "~~ Evaluates to true if the number of empty slots in your main backpack is less than 5 \nMainSlotsLE 5"
        ]
      },
      "MobsInDistance_Name": {
        "description": "Conditional block that evaluates to true if the number of mobs in the specified distance with names matching the specified pattern is greater than or equal to the specified count.",
        "arguments": [
          {
            "type": "Number",
            "description": "The count of mobs to compare."
          },
          {
            "type": "Decimal",
            "description": "The distance value to compare."
          },
          {
            "type": "RegexPattern",
            "description": "The regular expression pattern to match."
          }
        ]
      },
      "MobsInDistance_Priority": {
        "description": "Conditional block that evaluates to true if the number of mobs in the specified distance with priorities matching the specified pattern is greater than or equal to the specified count.",
        "arguments": [
          {
            "type": "Number",
            "description": "The count of mobs to compare."
          },
          {
            "type": "Decimal",
            "description": "The distance value to compare."
          },
          {
            "type": "Number",
            "description": "The priority value to match."
          }
        ]
      },
      "NavEmpty": {
        "description": "Conditional block that evaluates to true if the navigation system is currently empty.",
        "arguments": []
      },
      "NeedToBuff": {
        "description": "Conditional block that evaluates to true if the player character needs buffs.",
        "arguments": []
      },
      "Never": {
        "description": "Conditional block that never evaluates to true.",
        "arguments": []
      },
      "NoMobsInDistance": {
        "description": "Conditional block that evaluates to true if there are no mobs in the specified distance.",
        "arguments": [
          {
            "type": "Decimal",
            "description": "The distance value to compare."
          }
        ]
      },
      "Not": {
        "description": "Conditional block that evaluates to true if the nested condition is false.",
        "arguments": [
          {
            "type": "ConditionBlock",
            "description": "The nested condition to negate."
          }
        ]
      },
      "PSecondsInStateGE": {
        "description": "Conditional block that evaluates to true if the player has spent at least the specified number of seconds in the current state.",
        "arguments": [
          {
            "type": "Number",
            "description": "The seconds value to compare."
          }
        ]
      },
      "SecsInStateGE": {
        "description": "Conditional block that evaluates to true if the script has been in the current state for at least the specified number of seconds.",
        "arguments": [
          {
            "type": "Number",
            "description": "The seconds value to compare."
          }
        ]
      },
      "VendorClosed": {
        "description": "Conditional block that evaluates to true if the vendor window is closed.",
        "arguments": []
      },
      "VendorOpen": {
        "description": "Conditional block that evaluates to true if the vendor window is open.",
        "arguments": []
      }
  }
}