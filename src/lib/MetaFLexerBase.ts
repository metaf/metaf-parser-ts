import { Lexer, Token, CharStream, Interval } from "antlr4ng";
import { MetaFParser } from "../gen/MetaFParser";
import { Queue } from "./queue";
import { MetaFLexer } from "../gen/MetaFlexer";

export abstract class MetaFLexerBase extends Lexer {
  private _tokenQueue: Queue<Token>;
  private _nlToken: number;
  private _indentations: number[];

  private numberTypes: number[];
  private _eof: Token;

  constructor(input: CharStream) {
    super(input);
    this._tokenQueue = new Queue<Token>();
    this._nlToken = MetaFParser.NL; // Replace with the actual NL token type
    this._indentations = [0];
  
    this.numberTypes = [MetaFParser.NUMBER, MetaFParser.DECIMAL];
    this._eof = this._factory.create([null, null], MetaFLexer.EOF, "<EOF>", MetaFLexer.DEFAULT_TOKEN_CHANNEL, 0, 0, 0, 0);
  }

  nextToken(): Token {
    let t: Token | undefined;
    if (this._tokenQueue.size() > 0) {
      t = this._tokenQueue.dequeue();
      return t || this._eof;
    }
    else {
      t = super.nextToken();
    }

    if (t.type === MetaFParser.OPENCURL) {
      return this.handleCurlyString(t); 
    } else if (t.type === MetaFParser.NL) {
      return this.handleNewLine(t) || this._eof;
    }
    else if (t.type == MetaFParser.EOF) {
      return this.handleEOF(t) || this._eof;
    }

    return t || this._eof;
  }

  private handleCurlyString(t: Token): Token {
    const first = t;
    let allNumbers = true;
      let _nn = -1;
      let i = 1;
      let n = super.nextToken();

      let loop = 100000;
      do {
        _nn = this.inputStream.LA(i++);
        if (_nn != MetaFParser.CLOSECURL && !this.numberTypes.includes(_nn)) {
            allNumbers = false;
            break;
        }
      }
      while (_nn != MetaFParser.CLOSECURL && loop-- > 0);

      if (allNumbers && i > 2) {
        return t;
      }

      if (n.type === MetaFParser.CLOSECURL) {
        return this.createTokenFrom(first, MetaFParser.CURLYCONTENTS, "", n.stop);
      }
      else {
        let depth = 1;
        let start = n.start;
        let prev = n;

        let loop = 100000;
        while (depth > 0 && t.type >= 0 && loop-- > 0) {
          t = super.nextToken();
          if (!(prev.type === MetaFParser.ANY && prev.text === "\\")) {
            if (t.type === MetaFParser.OPENCURL) {
              depth++;
            } else if (t.type === MetaFParser.CLOSECURL) {
              depth--;
            }
          }
          prev = t;
        }

        let text = this._input.getText(Interval.of(start, t.stop))
            .replace("\\{", "{").replace("\\}", "}");
          text = text.substring(0, text.length - 1)
            .replace("{{", "{").replace("}}", "}")

        return this.createTokenFrom(first, MetaFParser.CURLYCONTENTS, text, t.stop + 1);
      }
  }

  private handleNewLine(t: Token) : Token | undefined {
    // fast forward to first non-nl token
    let nextNext = super.nextToken();
    let loop = 100000;
    while (nextNext.type == MetaFParser.NL && loop-- > 0) {
      t = nextNext;
      nextNext = super.nextToken();
    }

    if (nextNext.type == MetaFParser.EOF) {
      return this.handleEOF(nextNext);
    }

    let nlText = t.text || "";
    let indent = nlText.length - 1;
    if (indent > 0 && nlText[0] == '\r') {
      --indent;
    }
    let prevIndent = this._indentations[0];
    let r: Token | undefined;
    if (indent == prevIndent) {
      r = t;
    }
    else if (indent > prevIndent) {
      r = this.createTokenFrom(t, MetaFParser.INDENT, "", t.stop);
      this._indentations.unshift(indent);
    }
    else {
      r = this.unwindTo(indent, t);
    }

    this._tokenQueue.enqueue(nextNext);
    return r;
  }

  private handleEOF(t: Token): Token | undefined {
    let r: Token | undefined;
    if (this._indentations.length == 0) {
      r = this.createTokenFrom(t, MetaFParser.NL, "\n", t.stop);
    }
    else {
      r = this.unwindTo(0, t);
    }
    this._tokenQueue.enqueue(t);
    return r;
  }

  private unwindTo(targetIndent: number, copyFrom: Token) : Token | undefined {
    this._tokenQueue.enqueue(this.createTokenFrom(copyFrom, MetaFParser.NL, "\n", copyFrom.stop));

    let loop = 100000;
    while (true && loop-- > 0) {
      let prevIndent = this._indentations.shift();
      if (prevIndent == targetIndent || prevIndent == undefined) {
        break;
      }
      if (targetIndent > prevIndent) {
        this._indentations.unshift(prevIndent);
        this._tokenQueue.enqueue(this.createTokenFrom(copyFrom, MetaFParser.INDENT, copyFrom.text || "", copyFrom.stop));
        break;
      }
      this._tokenQueue.enqueue(this.createTokenFrom(copyFrom, MetaFParser.DEDENT, copyFrom.text || "", copyFrom.stop));
    }

    this._indentations.unshift(targetIndent);
    return this._tokenQueue.dequeue();
  }

  private createTokenFrom(original: Token, type: number, text: string, stop: number): Token {
    const newToken = this._factory.create([original.tokenSource, original.inputStream], type, text, original.channel, original.start, original.stop, original.line, original.column)
    newToken.stop = stop;
    return newToken;
  }
}