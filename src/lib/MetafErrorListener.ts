import { ANTLRErrorListener, Token, Recognizer, RecognitionException, InputMismatchException, RuleContext, DefaultErrorStrategy, NoViableAltException, FailedPredicateException, ParserRuleContext, ATNConfigSet, BitSet, DFA, Parser, ATNSimulator } from "antlr4ng";
import MetafModelBuilderVisitor from "../visitors/MetafModelBuilderVisitor";
import ParserHelpers from "./TokenValueResolvers";

export class MetafErrorListener implements ANTLRErrorListener {
    public Visitor: MetafModelBuilderVisitor;

    public Errors: MetafError[] = [];

    constructor(visitor: MetafModelBuilderVisitor) {
        this.Visitor = visitor;
    }

    syntaxError<S extends Token, T extends ATNSimulator>(recognizer: Recognizer<T>, offendingSymbol: S | null, line: number, charPositionInLine: number, msg: string, e: RecognitionException | null): void {
        let token = this.Visitor.TokenStream.get(this.Visitor.TokenStream.index);

        // we have to walk backwards on the token stream until we find a token with a valid position to
        // show the error at
        let i = 0;
        let loop = 100000;
		while (token.line == 0 && token.column == -1 && this.Visitor.TokenStream.index > i && loop-- > 0) {
			token = this.Visitor.TokenStream.get(this.Visitor.TokenStream.index - i++)
		}

        const _line = Math.max(0, token.line);
        const _column = Math.max(0, token.column);

        const rules = [];
        let ctx = this.Visitor.Parser.context;
        loop = 100000;
        while (ctx && loop-- > 0) {
            rules.push(ctx.ruleIndex);
            ctx = ctx.parent;
        }

        if (!e && this.Visitor.Parser.context) {
            //console.log(`message: ${msg}`);
            this.handleRuleArgumentsMismatch(recognizer, this.Visitor.Parser.context);
        }

        const start = {
            Line: _line,
            Column: _column,
            TokenIndex: this.Visitor.TokenStream.index
        } as MetafErrorPosition;

        if (typeof(offendingSymbol) == 'number') {
            this.Errors.push({
                Message: (msg ?? e?.message ?? "") + ` (number)`,
                Type: MetafErrorType.Error,
                Exception: e,
                RuleIndex: this.Visitor.Parser.context?.ruleIndex || 0,
                Context: this.Visitor.Parser.context,
                Start: start,
                End: undefined
            });
        }
        else {
            this.Errors.push({
                Message: (msg ?? e?.message ?? "") + ` (token)`,
                Type: MetafErrorType.Error,
                Exception: e,
                RuleIndex: this.Visitor.Parser.context?.ruleIndex || 0,
                Context: this.Visitor.Parser.context,
                Start: start,
                End: undefined
            });
        }
    }

    reportAmbiguity(recognizer: Parser, dfa: DFA, startIndex: number, stopIndex: number, exact: boolean, ambigAlts: BitSet, configs: ATNConfigSet): void {
        //throw new Error("Method not implemented.");
    }
    reportAttemptingFullContext(recognizer: Parser, dfa: DFA, startIndex: number, stopIndex: number, conflictingAlts: BitSet, configs: ATNConfigSet): void {
        //throw new Error("Method not implemented.");
    }
    reportContextSensitivity(recognizer: Parser, dfa: DFA, startIndex: number, stopIndex: number, prediction: number, configs: ATNConfigSet): void {
        //throw new Error("Method not implemented.");
    }

    handleInputMismatch(recognizer: any, e: InputMismatchException) {
        //if (this.handleRuleArgumentsMismatch(recognizer, e.context)) {
        //    return;
        //}

        //const message = `Expecting ${ParserHelpers.formatTokens(e.getExpectedTokens())}`;
        //e.message = message;
        //console.log(message)
        //this.notifyErrorListeners(recognizer, message, e);
    }

    handleRuleArgumentsMismatch(recognizer: any, ctx: RuleContext): boolean {
        return false;
    }
}

export class MetafErrorStrategy extends DefaultErrorStrategy {
    public Visitor: MetafModelBuilderVisitor;

    constructor(visitor: MetafModelBuilderVisitor) {
        super();
        this.Visitor = visitor;
    }

    public Errors: MetafError[] = [];

    reportError(recognizer: Parser, e: RecognitionException) {
        //super.reportError(recognizer, e);
        //return;
        try {
		let token = this.Visitor.TokenStream.get(this.Visitor.TokenStream.index);
		if (token.line == 0 && token.column == -1 && this.Visitor.TokenStream.index > 0) {
			token = this.Visitor.TokenStream.get(this.Visitor.TokenStream.index - 1)
		}

        const _line = Math.max(0, token.line);
        const _column = Math.max(0, token.column);

        const rules = [];
        let ctx = this.Visitor.Parser.context;
        let loop = 100000;
        while (ctx && loop-- > 0) {
            rules.push(ctx.ruleIndex);
            ctx = ctx.parent;
        }
        
        const offendingSymbol = e.offendingToken;
        let message = e.message ?? "Error";

        // if we've already reported an error and have not matched a token
        // yet successfully, don't report any errors.
        if (this.inErrorRecoveryMode(recognizer)) {
            //			System.err.print("[SPURIOUS] ");
            return; // don't report spurious errors
        }
        this.beginErrorCondition(recognizer);
        if (e instanceof NoViableAltException) {
            //console.error(`NoViableAltException exception...`)
            this.reportNoViableAlternative(recognizer, e);
        }
        else if (e instanceof InputMismatchException) {
            //console.error(`InputMismatchException exception...`)
            //this.handleInputMismatch(recognizer, e)
            this.reportInputMismatch(recognizer, e)
        }
        else if (e instanceof FailedPredicateException) {
            //console.error(`FailedPredicateException exception...`)
            this.reportFailedPredicate(recognizer, e);
        }
        else {
            //console.error(`unknown recognition error type: ${e}`);
            //this.notifyErrorListeners(recognizer, e.toString(), e);
            //this.
        }
    } catch (e) {
        console.error(e)
    }
    }
/*
    getMissingSymbol(recognizer) {
        let currentSymbol = recognizer.currentToken;
        let expecting = this.getExpectedTokens(recognizer);
        let expectedTokenType = Token.INVALID_TYPE;
        if (!expecting.isNil) {
            // get any element
            expectedTokenType = expecting.minElement;
        }
        let tokenText;
        if (expectedTokenType === Token.EOF) {
            tokenText = "<missing EOF>";
        }
        else {
            //tokenText = "<missing " + recognizer.vocabulary.getDisplayName(expectedTokenType) + ">";
            tokenText = "";
        }
        let current = currentSymbol;
        let lookback = recognizer.inputStream.tryLT(-1);
        if (current.type === Token.EOF && lookback != null) {
            current = lookback;
        }
        this.
        return this.constructToken(recognizer.inputStream.tokenSource, expectedTokenType, tokenText, current);
    }
    */

    reportMetafError(error: MetafError) {
        this.Errors.push(error);
    }
}

export interface MetafError {
    Message: string;
    Exception: RecognitionException | undefined;
    Type: MetafErrorType;
    RuleIndex: number;
    Context: ParserRuleContext | undefined;
    Start: MetafErrorPosition;
    End: MetafErrorPosition | undefined;
}

export interface MetafErrorPosition {
    Line: number;
    Column: number;
    TokenIndex: number | undefined;
}

export enum MetafErrorType {
  Unknown,
  Error,
  Warning,
  Deprecated,
  ParseError,
  InvalidArgumentType,
  InvalidArgumentCount,
  UnexpectedIdentifier,
  ChildrenNotSupported,
  DuplicateDefinition,
  InvalidChild,
  InvalidBlock
}