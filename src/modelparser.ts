import MetafModelBuilderVisitor from './visitors/MetafModelBuilderVisitor';
import { Meta } from './models/Meta';
import { MetafErrorListener, MetafErrorStrategy } from './lib/MetafErrorListener';

/**
 * ModelParser class for parsing Metaf models.
 */
export default class ModelParser {
  public Visitor: MetafModelBuilderVisitor;
  meta: Meta;

  /**
   * The error handler used for lexing / parsing.
   */
  get ErrorHandler(): MetafErrorListener {
    return this.Visitor.ErrorListener;
  }
  
  /**
   * The error handler used for lexing / parsing.
   */
  get ErrorStrategy(): MetafErrorStrategy {
    return this.Visitor.ErrorStrategy;
  }

  /**
   * Create a new Metaf parser from the string contents of an .af file.
   * @param contents - The string contents of a Metaf .af file.
   */
  constructor(filename: string, contents: string) {
    this.Visitor = new MetafModelBuilderVisitor(filename, contents);
  }

  /**
   * Try and parse the meta.
   * @returns Object containing the parsed meta and success status, or null if parsing failed.
   */
  tryParse(): { meta: Meta | null; success: boolean } {
    this.meta = this.Visitor.build();
    return {
      meta: this.meta,
      success: !this.hasErrors(),
    };
  }

  /**
   * Checks if any errors were encountered while parsing / building models.
   * @returns True if there were errors, false otherwise.
   */
  hasErrors(): boolean {
    return this.ErrorHandler.Errors.length > 0;
  }
}