# Metaf Parser

metaf-parser-ts is a typescript library that utilizes ANTLR4 grammars to parse scripts written in the Metaf language to an easy to work with model format.

## Features

- **ANTLR4 Grammar:** The project includes a comprehensive ANTLR4 grammar for the Metaf language, defining the syntax and structure of the scripts.

- **Model Parser:** A typescript class (`ModelParser`) with a simplified interface for turning meta file contents into a `Meta` model.

- **Meta Model:** The `Meta` model represents the structured information extracted from Metaf scripts, including states, rules, conditions, actions, nav routes, and more.

## Installation

`npm install metaf-parser-ts`

## Example

```typescript
import { ModelParser } from 'metaf-parser-ts'

const afContents = "..."; // Your Metaf script here
const parser = new ModelParser(afContents);
const parseResult = parser.tryParse();

if (!parseResult.success || parseResult.meta == null) {
	// check parser.ErrorHandler.Errors for a list of parser errors.
}
else {
	// Now you can use the 'meta' object to access the parsed information
	for (const state of parseResult.meta.States) {
		// do stuff
	}
}
```

## Updating the grammar

To update the grammar, update the lexer/parser .g4 grammar files and run `npm run antlr4ts`. The updated parser / lexer code should be automatically generated to the `./gen/` directory.

You also need to manually add the following line to the top of the generated `./gen/MetaFlexer.ts` file:
```typescript
import { MetaFLexerBase } from "./../src/lib/MetaFLexerBase";
```

## Building release

```
npm run build
```

## Contributions

Contributions are welcome! If you find any issues, have suggestions, or want to improve the project, feel free to submit a pull request or open an issue on the GitLab repository.


## License

This project is licensed under the MIT License.
